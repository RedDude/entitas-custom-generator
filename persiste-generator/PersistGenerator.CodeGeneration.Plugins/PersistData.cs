﻿using DesperateDevs.CodeGeneration;

namespace CoolGenerator.CodeGeneration.Plugins
{
    public class PersistData : CodeGeneratorData
    {    
        // string keys to access the base dictionary with
        public const string NameKey = "Persisty.Name";
        
        // wrapper property to access the dictionary
        public string Name
        {
            get => (string)this[NameKey];
            set => this[NameKey] = value;
        }
    }
}