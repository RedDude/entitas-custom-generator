using System.Linq;
using DesperateDevs.CodeGeneration;
using Entitas.CodeGeneration.Plugins;

namespace CoolGenerator.CodeGeneration.Plugins
{
    public class PersistGenerator : ICodeGenerator
    {
        public string name => "Persist";
        public int priority => 0;
        public bool runInDryMode => true;

        private const string DIRECTORY_NAME = "Persist";

        private const string COMPONENT_TEMPLATE =
            @"    
        deserializer.Add(""${ComponentName}"", (byteData) => Deserialize<${ComponentType}>(byteData));
        deserializerJson.Add(""${ComponentName}"", (json) => DeserializeFromJson<${ComponentType}>(json));
        serializer.Add(""${ComponentName}"", component => Serialize((${ComponentType})component));
        serializerJson.Add(""${ComponentName}"", (component) => SerializeToJson((${ComponentType})component));
";

        private const string TEMPLATE =
@"using MessagePack;
using System;
using System.Collections.Generic;
using Entitas;

public class MessagePackComponentLookup
{ 
    public Dictionary<string, Func<byte[], IComponent>> deserializer = new Dictionary<string, Func<byte[], IComponent>>();
    public Dictionary<string, Func<string, IComponent>> deserializerJson = new Dictionary<string, Func<string, IComponent>>();
    public Dictionary<string, Func<object, byte[]>> serializer = new Dictionary<string, Func<object, byte[]>>();
    public Dictionary<string, Func<object, string>> serializerJson = new Dictionary<string, Func<object, string>>();

    public byte[] Serialize<T>(T obj, MessagePackSerializerOptions lz4Options = null)
    {
        return MessagePackSerializer.Serialize(obj, lz4Options);
    }
    
    public string SerializeToJson<T>(T obj, MessagePackSerializerOptions lz4Options = null)
    {
        return MessagePackSerializer.SerializeToJson(obj, lz4Options);
    }

    public T Deserialize<T>(byte[] bytes, MessagePackSerializerOptions lz4Options = null)
    {
        return MessagePackSerializer.Deserialize<T>(bytes, lz4Options);
    }
    
    public T DeserializeFromJson<T>(string json, MessagePackSerializerOptions lz4Options = null)
    {
        var bytes = MessagePackSerializer.ConvertFromJson(json, lz4Options);
        return Deserialize<T>(bytes);
    }

    public MessagePackComponentLookup() {
        ${componentsList}
    }
}
";

        public CodeGenFile[] Generate(CodeGeneratorData[] data)
        {
            var persist = data
                .OfType<PersistData>()
                .ToArray();

            return new[] {Generate(persist)};
        }

        CodeGenFile Generate(PersistData[] data)
        {
            var componentList = string.Join("\n", data
                .Select((d, index) => COMPONENT_TEMPLATE
                    .Replace("${ComponentName}", d.Name.ToComponentName(true))
                    .Replace("${ComponentType}", d.Name)));

            var fileContent = TEMPLATE
                .Replace("${componentsList}", componentList);

            return new CodeGenFile("MessagePackComponentLookup.cs",
                fileContent,
                GetType().FullName
            );
        }
    }
}