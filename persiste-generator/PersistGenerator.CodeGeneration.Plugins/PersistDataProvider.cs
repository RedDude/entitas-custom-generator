using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DesperateDevs.CodeGeneration;
using DesperateDevs.CodeGeneration.Plugins;
using DesperateDevs.Roslyn;
using DesperateDevs.Serialization;
using Entitas;
using Microsoft.CodeAnalysis;

namespace CoolGenerator.CodeGeneration.Plugins
{
    public class PersistDataProvider : IDataProvider, IConfigurable, ICachable
    {
        public string name => "Persist"; // set this to match your dataprovider name
        public int priority => 0; // can be left at 0
        public bool runInDryMode => true; // allows this to run in dry-run mode, optional
        
        public Dictionary<string, string> defaultProperties => _projectPathConfig.defaultProperties;
        public Dictionary<string, object> objectCache { get; set; }        
        private readonly ProjectPathConfig _projectPathConfig = new ProjectPathConfig();
        
        public void Configure(Preferences preferences)
        {
            _projectPathConfig.Configure(preferences);
        }

        // Define here which attribute you want to look for and the data type of the 
        // code gen data we will create
    
        public CodeGeneratorData[] GetData()
        {
            var names = new List<string>();

            var attr = DesperateDevs.Roslyn.CodeGeneration.Plugins.PluginUtil
                .GetCachedProjectParser(objectCache, _projectPathConfig.projectPath)
                .GetTypes();
            
            foreach (var namedTypeSymbol in attr)
            {
                // names.Add("* " + namedTypeSymbol.BaseType.MetadataName);
                
                foreach (var @interface in namedTypeSymbol.AllInterfaces)
                {
                    names.Add(@interface.MetadataName);
                }
            }
       
            // // WriteAllLines creates a file, writes a collection of strings to the file,
            // // and then closes the file.  You do NOT need to call Flush() or Close().
            System.IO.File.WriteAllLines(@"C:\log\log.txt", names.ToArray());
            
            return DesperateDevs.Roslyn.CodeGeneration.Plugins.PluginUtil
                .GetCachedProjectParser(objectCache, _projectPathConfig.projectPath)
                .GetTypes()
                .Where(type => ImplementsInterface(type, "IComponent"))
                .Where(type => type.GetAttributes().Where(data => data.AttributeClass.Name == "MessagePackObjectAttribute").Count() > 0) // change to your attribute
                .Select(type => new PersistData // change to your data type
                {
                    Name = type.Name,
                    
                }).ToArray();
        }
      
          
        public static bool ImplementsInterface(INamedTypeSymbol typeSymbol, string interfaceName)
        {
            if (typeSymbol == null)
            {
                return false;
            }

            foreach (var @interface in typeSymbol.AllInterfaces)
            {
                if (@interface.MetadataName == interfaceName)
                {
                    return true;
                }
            }

            return false;
        }
    }
}