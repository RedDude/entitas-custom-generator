﻿using System;

namespace CoolGenerator.CodeGeneration.Attributes
{
    // change attribute usage if desired, 
    // this defines where the attribute is allowed to be placed
    // e.g. on a class, field, property, method etc
    [AttributeUsage(AttributeTargets.Class)]
    public class CoolAttribute : Attribute
    {
    }
}