using System.Linq;
using DesperateDevs.CodeGeneration;
using DesperateDevs.Utils;
using Entitas.CodeGeneration.Plugins;
using System.IO;

namespace CoolGenerator.CodeGeneration.Plugins
{
    public class CoolGenerator : ICodeGenerator
    {
        public string name => "Cool";
        public int priority => 0;
        public bool runInDryMode => true;

        private const string DIRECTORY_NAME = "CoolStuff";

        private const string STANDARD_TEMPLATE =
@"using UnityEngine;

public class ${ClassName} : MonoBehaviour
{ 
${Fields} 

    public void Initialize(GameEntity entity) 
    {
        entity.Replace${ComponentName}(${Args});
    }
}
";
        private const string FLAG_TEMPLATE =
@"using UnityEngine;

public class ${ClassName} : MonoBehaviour
{        
    public void Initialize(GameEntity entity) 
    {
        entity.is${ComponentName} = true;
    }
}
";
        private const string FieldTemplate = @"    public ${FieldType} ${fieldName};";
        private const string ArgTemplate = @"${fieldName}";

        public CodeGenFile[] Generate(CodeGeneratorData[] data)
        {            
            return data
                .OfType<CoolData>() // Change to your data type                
                .Select(Generate)
                .ToArray();
        }

        private CodeGenFile Generate(CoolData data)
        {
            // helpful extention methods for stripping the Component suffix if it exists
            var componentName = data.Name.ToComponentName(true);
            // here you define how the name of your generated file relates to the name of the component
            var className = componentName + "Script";  
            // here you set your file name
            var filename = DIRECTORY_NAME + Path.DirectorySeparatorChar + className + ".cs";

            var memberData = data.MemberData;
            var template = memberData.ToArray().Length == 0 ? FLAG_TEMPLATE : STANDARD_TEMPLATE;

            var fileContent = template
                .Replace("${ClassName}", className)
                .Replace("${ComponentName}", componentName)
                .Replace("${Fields}", GenerateFields(data.MemberData))
                .Replace("${Args}", GenerateArgs(data.MemberData));

            return new CodeGenFile(
                filename,
                fileContent,
                GetType().FullName);
        }

        private static string GenerateFields(MemberData[] data)
        {
            return string.Join("\n", data.Select(member => FieldTemplate
                    .Replace("${FieldType}", member.type)
                    .Replace("${fieldName}", member.name.LowercaseFirst())
                    .Replace("${FieldName}", member.name))
                .ToArray());
        }

        private static string GenerateArgs(MemberData[] data)
        {
            return string.Join(", ", data.Select(member => ArgTemplate
                    .Replace("${FieldType}", member.type)
                    .Replace("${fieldName}", member.name.LowercaseFirst())
                    .Replace("${FieldName}", member.name))
                .ToArray());
        }
    }
}