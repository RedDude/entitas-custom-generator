﻿using DesperateDevs.CodeGeneration;
using Entitas.CodeGeneration.Plugins;

namespace CoolGenerator.CodeGeneration.Plugins
{
    public class CoolData : CodeGeneratorData
    {    
        // string keys to access the base dictionary with
        public const string NameKey = "Cool.Name";
        public const string DataKey = "Cool.Data";
        
        // wrapper property to access the dictionary
        public string Name
        {
            get => (string)this[NameKey];
            set => this[NameKey] = value;
        }

        // wrapper properties to access the dictionary
        public MemberData[] MemberData
        {
            get => (MemberData[])this[DataKey];
            set => this[DataKey] = value;
        }         
    }
}