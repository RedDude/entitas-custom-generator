using System.Collections.Generic;
using System.Linq;
using CoolGenerator.CodeGeneration.Attributes;
using DesperateDevs.CodeGeneration;
using DesperateDevs.CodeGeneration.Plugins;
using DesperateDevs.Roslyn;
using DesperateDevs.Serialization;
using Entitas.CodeGeneration.Plugins;
using Microsoft.CodeAnalysis;

namespace CoolGenerator.CodeGeneration.Plugins
{
    public class CoolDataProvider : IDataProvider, IConfigurable, ICachable
    {
        public string name => "Cool"; // set this to match your dataprovider name
        public int priority => 0; // can be left at 0
        public bool runInDryMode => true; // allows this to run in dry-run mode, optional
        
        public Dictionary<string, string> defaultProperties => _projectPathConfig.defaultProperties;
        public Dictionary<string, object> objectCache { get; set; }        
        private readonly ProjectPathConfig _projectPathConfig = new ProjectPathConfig();
        
        public void Configure(Preferences preferences)
        {
            _projectPathConfig.Configure(preferences);
        }

        // Define here which attribute you want to look for and the data type of the 
        // code gen data we will create
        public CodeGeneratorData[] GetData()
        {
            return DesperateDevs.Roslyn.CodeGeneration.Plugins.PluginUtil
                .GetCachedProjectParser(objectCache, _projectPathConfig.projectPath)
                .GetTypes()
                .Where(type => type.GetAttribute<CoolAttribute>() != null) // change to your attribute
                .Select(type => new CoolData // change to your data type
                {
                    Name = type.Name,
                    MemberData = GetData(type)
                }).ToArray();
        }

        // this will get field type names and field names for each field in the class
        // replacing IFieldSymbol with IPropertySymbol would get property names and types instead
        private MemberData[] GetData(INamedTypeSymbol type)
        {
            return type.GetMembers()
                .OfType<IFieldSymbol>() 
                .Select(field => new MemberData(field.Type.ToCompilableString(), field.Name.Split('>')[0].Replace("<","")))
                .ToArray();
        }
    }
}