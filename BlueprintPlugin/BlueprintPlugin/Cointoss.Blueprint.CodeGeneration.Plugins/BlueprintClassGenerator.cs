﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using DesperateDevs.CodeGeneration;
using Entitas.CodeGeneration.Plugins;

namespace Cointoss.Blueprint.CodeGeneration.Plugins
{
	internal sealed class BlueprintClassGenerator : ICodeGenerator
	{
		public string name => NAME;

		public int priority => DEFAULT_PRIORITY;

		public bool runInDryMode => DEFAULT_DRY_RUN_MODE;

		private const string NAME = "Blueprint Class Generator";
		private const int DEFAULT_PRIORITY = 0;
		private const bool DEFAULT_DRY_RUN_MODE = true;

		// Code-generation format strings
		private const string BLUEPRINT_BEHAVIOUR_CLASS_TEMPLATE = @"
using JCMG.EntitasRedux.Blueprints;
using UnityEngine;

/// <summary>
/// Represents a group of <see cref=""JCMG.EntitasRedux.IComponent""/> instances that can be copied to one or more
/// <see cref=""${ContextName}Entity""/>.
/// </summary>
[AddComponentMenu(""Blueprints/${ContextName}/${ContextName}BlueprintBehaviour"")]
public partial class ${ContextName}BlueprintBehaviour : BlueprintBehaviourBase, I${ContextName}Blueprint
{
	/// <summary>
	/// Applies all components in the blueprint to <paramref name=""entity""/>.
	/// </summary>
	/// <param name=""entity""></param>
	public void CopyComponentTo(${ContextName}Entity entity)
	{
		for (var i = 0; i < _components.Count; i++)
		{
			var component = _components[i];
			if(_components[i] == null)
			{
				continue;
			}

			var index = ${ContextName}ComponentsTypeToIndexLookup.GetComponentIndex(component);
			if (index != -1)
			{
				entity.AddComponent(index, component);
			}
			else
			{
				Debug.LogWarningFormat(
					BlueprintConstants.COMPONENT_INDEX_DOES_NOT_EXIST_FOR_TYPE_FORMAT,
					component.GetType(),
					typeof(${ContextName}ComponentsLookup));
			}
		}
	}

	protected override void OnValidate()
	{
		base.OnValidate();

		// Remove any components that no longer belong to this context.
		for (var i = _components.Count - 1; i >= 0; i--)
		{
			var index = ${ContextName}ComponentsTypeToIndexLookup.GetComponentIndex(_components[i]);
			if (index == -1)
			{
				_components.RemoveAt(i);
			}
		}
	}
}
";

		private const string BLUEPRINT_CONFIG_CLASS_TEMPLATE = @"
using System;
using System.Collections.Generic;
using System.Linq;
using Entitas;
using JCMG.EntitasRedux.Blueprints;
using UnityEngine;

/// <summary>
/// Represents a group of <see cref=""IComponent""/> instances that can be copied to one or more
/// <see cref=""${ContextName}Entity""/>.
/// </summary>
[CreateAssetMenu(fileName = ""New${ContextName}Blueprint"", menuName = ""Blueprints/${ContextName}/${ContextName}Blueprint"")]
public partial class ${ContextName}Blueprint : BlueprintBase, I${ContextName}Blueprint, ISerializationCallbackReceiver
{
	private static readonly List<string> componentNames = ${ContextName}ComponentsLookup.componentNames.ToList();

	/// <summary>
	/// Copy all components in the blueprint to <paramref name=""entity""/>.
	/// </summary>
	/// <param name=""entity""></param>
	public void CopyComponentTo(${ContextName}Entity entity)
	{
		for (var i = 0; i < _components.Count; i++)
		{
			var component = _components[i];
			if(_components[i] == null)
			{
				continue;
			}

			entity.CopyComponentTo(component);
		}
	}

	protected override void OnValidate()
	{
		base.OnValidate();

		// Remove any components that no longer belong to this context.
		for (var i = _components.Count - 1; i >= 0; i--)
		{
			var component = _components[i];
			var index = ${ContextName}ComponentsTypeToIndexLookup.GetComponentIndex(_components[i]);
			if (index == -1)
			{
				Debug.Log($@""Could not find component - {component.ToString()} - in the - ${ContextName}"");
				_components.RemoveAt(i);
			}
		}
	}

	public void OnBeforeSerialize()
	{
		_flagComponents.Clear();
		foreach (var component in _components)
		{
			if (IsFlagComponent(component))
			{
				var s = component.GetType().Name;
				var componentName = s.Remove(s.Length - 9, 9);
				if(!_flagComponents.Contains(componentName))
					_flagComponents.Add(componentName);
			}
		}
	}

	public void OnAfterDeserialize()
	{
		OnValidate();
		foreach (var flagComponent in _flagComponents)
		{
			var index = componentNames.IndexOf(flagComponent);
			
			var componentType = ${ContextName}ComponentsLookup.componentTypes[index];
			if (!HasComponentType(_components, componentType))
			{
				var newComponent = (IComponent)Activator.CreateInstance(componentType);
				_components.Add(newComponent);
			}
		}
	}
}
";

		private const string BLUEPRINT_INTERFACE_TEMPLATE = @"
/// <summary>
/// Represents a group of <see cref=""IComponent""/> instances that can be copied to one or more
/// <see cref=""${ContextName}Entity""/>.
/// </summary>
public interface I${ContextName}Blueprint
{
	/// <summary>
	/// Applies all components in the blueprint to <paramref name=""entity""/>.
	/// </summary>
	/// <param name=""entity""></param>
	void CopyComponentTo(${ContextName}Entity entity);
}
";

		// Find and replace tokens
		private const string CONTEXT_NAME_TOKEN = "${ContextName}";

		private const string BLUEPRINT_BEHAVIOUR_FILENAME_FORMAT = "${ContextName}BlueprintBehaviour.cs";
		private const string BLUEPRINT_CONFIG_FILENAME_FORMAT = "${ContextName}Blueprint.cs";
		private const string BLUEPRINT_INTERFACE_FILENAME_FORMAT = "I${ContextName}Blueprint.cs";

		public CodeGenFile[] Generate(CodeGeneratorData[] data)
		{
			var codeGenFilesResult = new List<CodeGenFile>();

			// Create a blueprint class per context
			var allContextData = data.OfType<ContextData>().ToArray();
			for (var i = 0; i < allContextData.Length; i++)
			{
				var contextData = allContextData[i];
				var contextName = contextData.GetContextName();

				// Create Blueprint Interface
				var interfaceFilename = BLUEPRINT_INTERFACE_FILENAME_FORMAT.Replace(CONTEXT_NAME_TOKEN, contextName);
				var interfaceAbsoluteFilename = Path.Combine(contextName, interfaceFilename);
				var interfaceFileContents = BLUEPRINT_INTERFACE_TEMPLATE.Replace(CONTEXT_NAME_TOKEN, contextName);
				codeGenFilesResult.Add(new CodeGenFile(interfaceAbsoluteFilename, interfaceFileContents, NAME));

				// Create Blueprint ScriptableObject
				var blueprintFilename = BLUEPRINT_CONFIG_FILENAME_FORMAT.Replace(CONTEXT_NAME_TOKEN, contextName);
				var blueprintAbsoluteFilename = Path.Combine(contextName, blueprintFilename);
				var blueprintFileContents = BLUEPRINT_CONFIG_CLASS_TEMPLATE.Replace(CONTEXT_NAME_TOKEN, contextName);
				codeGenFilesResult.Add(new CodeGenFile(blueprintAbsoluteFilename, blueprintFileContents, NAME));

				// Create Blueprint MonoBehaviour
				var behaviourFilename = BLUEPRINT_BEHAVIOUR_FILENAME_FORMAT.Replace(CONTEXT_NAME_TOKEN, contextName);
				var behaviourAbsoluteFilename = Path.Combine(contextName, behaviourFilename);
				var behaviourFileContents = BLUEPRINT_BEHAVIOUR_CLASS_TEMPLATE.Replace(CONTEXT_NAME_TOKEN, contextName);
				codeGenFilesResult.Add(new CodeGenFile(behaviourAbsoluteFilename, behaviourFileContents, NAME));
			}

			return codeGenFilesResult.ToArray();
		}
	}
}