using System.IO;
using System.Linq;
using DesperateDevs.CodeGeneration;
using Entitas.CodeGeneration.Plugins;

namespace CoolGenerator.CodeGeneration.Plugins
{
    public class Generator : ICodeGenerator
    {
        public string name => "ListenerView";
        public int priority => 0;
        public bool runInDryMode => true;

        private const string DIRECTORY_NAME = "ListenerViews";

        private const string TEMPLATE =
@"using Entitas;
using UnityEngine;

public class ${ComponentName}ViewListener : MonoBehaviour, IEventListener, I${ComponentName}Listener {
    
    GameEntity _entity;
 
    public void RegisterEventListeners(IEntity entity) {
        _entity = (GameEntity)entity;
        _entity.Add${ComponentName}Listener(this);
    }
}
";

        public CodeGenFile[] Generate(CodeGeneratorData[] data)
        {            
            return data
                .OfType<ListenerViewData>() // Change to your data type                
                .Select(Generate)
                .ToArray();
        }

        private CodeGenFile Generate(ListenerViewData viewData)
        {
            var componentName = viewData.Name.ToComponentName(true);
            var className = componentName + "ViewListener";  
            var filename = DIRECTORY_NAME + Path.DirectorySeparatorChar + className + ".cs";

            var fileContent = TEMPLATE
                .Replace("${ClassName}", className)
                .Replace("${ComponentName}", componentName);

            return new CodeGenFile(
                filename,
                fileContent,
                GetType().FullName);
        }
    }
}