// using System.Linq;
// using DesperateDevs.CodeGeneration;
// using Entitas.CodeGeneration.Plugins;
//
// namespace CoolGenerator.CodeGeneration.Plugins
// {
//     public class BetterPersistGenerator
//     {
//         public string name => "Persist";
//         public int priority => 0;
//         public bool runInDryMode => true;
//
//         private const string DIRECTORY_NAME = "Persist";
//         
//         private const string TEMPLATE =
//             @"using System;
// using System.Collections.Generic;
// using Entitas;
//
// public class PersistenceComponentLookup
// { 
//     public PersistenceComponentLookup(
//         ISerializer serializer,
//         ISerializer serializerJson,
//         IDeserializer deserializer,
//         IDeserializer deserializerJson,
//         Dictionary<string, Func<object, byte[]>> serializerTypeLookup,
//         Dictionary<string, Func<object, string>> serializerJsonTypeLookup,
//         Dictionary<string, Func<byte[], IComponent>> deserializerTypeLookup,
//         Dictionary<string, Func<string, IComponent>> deserializerJsonTypeLookup) {
//
//         ${componentsList}
//     }
// }
// ";
//
//         private const string COMPONENT_TEMPLATE =
//         @"    
//         serializerTypeLookup.Add(""${ComponentName}"", component => serializer.Serialize((${ComponentType})component));
//         serializerJsonTypeLookup.Add(""${ComponentName}"", (component) => serializerJson.Serializer((${ComponentType})component));
//         deserializerTypeLookup.Add(""${ComponentName}"", (byteData) => deserializer.Deserialize<${ComponentType}>(byteData));
//         deserializerJsonTypeLookup.Add(""${ComponentName}"", (json) => deserializerJson.Deserialize<${ComponentType}>(json));";
//
//
//         public CodeGenFile[] Generate(CodeGeneratorData[] data)
//         {
//             var persist = data
//                 .OfType<PersistData>()
//                 .ToArray();
//
//             return new[] {Generate(persist)};
//         }
//
//         CodeGenFile Generate(PersistData[] data)
//         {
//             var componentList = string.Join("\n", data
//                 .Select((d, index) => COMPONENT_TEMPLATE
//                     .Replace("${ComponentName}", d.Name.ToComponentName(true))
//                     .Replace("${ComponentType}", d.Name)));
//
//             var fileContent = TEMPLATE
//                 .Replace("${componentsList}", componentList);
//
//             return new CodeGenFile("PersistenceComponentLookup.cs",
//                 fileContent,
//                 GetType().FullName
//             );
//         }
//     }
// }