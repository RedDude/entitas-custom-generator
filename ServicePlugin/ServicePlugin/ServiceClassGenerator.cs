﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using DesperateDevs.CodeGeneration;
using DesperateDevs.Utils;
using Entitas.CodeGeneration.Plugins;

namespace Cointoss.Service.CodeGeneration.Plugins
{
	internal sealed class ServiceClassGenerator : ICodeGenerator
	{
		public string name => NAME;

		public int priority => DEFAULT_PRIORITY;

		public bool runInDryMode => DEFAULT_DRY_RUN_MODE;

		private const string NAME = "Service Class Generator";
		private const int DEFAULT_PRIORITY = 0;
		private const bool DEFAULT_DRY_RUN_MODE = true;

		// Code-generation format strings
		private const string SERVICES_CLASS_TEMPLATE = @"
public class Services
{
    ${servicesVars}
}
";

		private const string REGISTER_SERVICE_CLASS_TEMPLATE = @"
using Entitas;
using Sources;

public class ${name}RegisterServiceSystem : IInitializeSystem
{
    private readonly ServiceContext _serviceContext;
    private readonly I${name}Service _service;

    public ${name}RegisterServiceSystem(Contexts contexts, I${name}Service service)
    {
        _serviceContext = contexts.service;
        _service = service;
    }

    public void Initialize()
    {
        _serviceContext.Replace${name}Service(_service);
    }
}

public static class Context${name}ServiceExtension {
	public static I${name}Service Get${name}Service(this ServiceContext service)
    {
        return service.${nameLower}Service.instance;
    }

    public static void Add(this ServiceRegistrationSystemsFeature feature, I${name}Service service)
    {
        feature.services.${nameLower} = service;
        feature.Add(new ${name}RegisterServiceSystem(Contexts.sharedInstance, service));
    }
}

";

		private const string SERVICE_INTERFACE_TEMPLATE = @"
public partial interface I${name}Service
{
}
";
		
private const string SERVICE_FIELD_TEMPLATE = @"
	public I${name}Service ${nameLower};
";

		// Find and replace tokens
		private const string NAME_TOKEN = "${name}";

		private const string SERVICE_CLASS_FILENAME_FORMAT = "Services.cs";
		private const string SERVICE_REGISTOR_FILENAME_FORMAT = "${name}RegisterServiceSystem.cs";
		private const string SERVICE_INTERFACE_FILENAME_FORMAT = "I${name}Service.cs";

		public CodeGenFile[] Generate(CodeGeneratorData[] data)
		{
			var codeGenFilesResult = new List<CodeGenFile>();

			var allServiceComponents = data.OfType<ComponentData>().Where(cdata => cdata.GetContextNames().Contains("Service")).ToArray();
			var servicesNames = new List<string>();
			for (var i = 0; i < allServiceComponents.Length; i++)
			{
				var componentData = allServiceComponents[i];
				var componentName = componentData.GetTypeName().Replace("Service", "").Replace("Component", "");
				
				var field = SERVICE_FIELD_TEMPLATE.Replace(NAME_TOKEN, componentName)
					.Replace("${nameLower}", componentName.LowercaseFirst());
				
				servicesNames.Add(field);

				
				var interfaceFilename = SERVICE_INTERFACE_FILENAME_FORMAT.Replace(NAME_TOKEN, componentName);
				var interfaceAbsoluteFilename = Path.Combine(componentName, interfaceFilename);
				var interfaceFileContents = SERVICE_INTERFACE_TEMPLATE.Replace(NAME_TOKEN, componentName);
				codeGenFilesResult.Add(new CodeGenFile(interfaceAbsoluteFilename, interfaceFileContents, NAME));

				var registorFilename = SERVICE_REGISTOR_FILENAME_FORMAT.Replace(NAME_TOKEN, componentName);
				var registorAbsoluteFilename = Path.Combine(componentName, registorFilename);
				var registorFileContents = REGISTER_SERVICE_CLASS_TEMPLATE.Replace(NAME_TOKEN, componentName)
					.Replace("${nameLower}", componentName.LowercaseFirst());
				codeGenFilesResult.Add(new CodeGenFile(registorAbsoluteFilename, registorFileContents, NAME));
			}

			var behaviourFileContents = SERVICES_CLASS_TEMPLATE.Replace("${servicesVars}", string.Join("", servicesNames));
			codeGenFilesResult.Add(new CodeGenFile(SERVICE_CLASS_FILENAME_FORMAT, behaviourFileContents, NAME));

			return codeGenFilesResult.ToArray();
		}
	}
}