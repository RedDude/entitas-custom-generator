﻿using System;
using Entitas;

namespace Cointoss.ComponentAccessor.CodeGeneration.Plugins
{
    public class ComponentAccessor : IComponentAccessor
    {
        public Func<IComponent, object> _GetValue;
        public Action<IComponent, object> _SetValue;

        public Type fieldType;
        // public void GetValue(T component) => ((T) component).value,
        // public SetValue = (component, value) => ((IdComponent) component).value = (string) value,

        public ComponentAccessor(Func<IComponent, object> getValue, Action<IComponent, object> setValue)
        {
            _GetValue = getValue;
            _SetValue = setValue;
        }

        public object GetValue(IComponent comp)
        {
            return _GetValue.Invoke(comp);
        }

        public void SetValue(IComponent comp, object value)
        {
            _SetValue.Invoke(comp, value);
        }
    }
    
    public class ComponentAccessorInt : IComponentAccessor
    {
        public Func<IComponent, int> _GetValue;
        public Action<IComponent, int> _SetValue;

        public Type fieldType;
        // public void GetValue(T component) => ((T) component).value,
        // public SetValue = (component, value) => ((IdComponent) component).value = (string) value,

        public ComponentAccessorInt(Func<IComponent, int> getValue, Action<IComponent, int> setValue)
        {
            _GetValue = getValue;
            _SetValue = setValue;
        }
        
        public int GetIntValue(IComponent comp)
        {
            return _GetValue.Invoke(comp);
        }

        public object GetValue(IComponent comp)
        {
            return _GetValue.Invoke(comp);
        }

        public void SetValue(IComponent comp, object value)
        {
            _SetValue.Invoke(comp, (int) value);
        }
    }
    
    public interface IComponentAccessor
    {
        object GetValue(IComponent comp);
        void SetValue(IComponent comp, object value);
    }
}