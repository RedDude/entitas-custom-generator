﻿using System.Collections.Generic;
using System.Linq;
using DesperateDevs.CodeGeneration;
using Entitas.CodeGeneration.Plugins;

namespace Cointoss.ComponentAccessor.CodeGeneration.Plugins
{
	internal sealed class ComponentMemberDataGenerator : AbstractGenerator
	{
		public override string name => "ComponentMemberData (Lookup)";

		private const string TEMPLATE =
			@"using System;
using System.Collections.Generic;
using Entitas;

public static class ${Lookup} {

	public static readonly Dictionary<Type, ComponentMemberData> componentMembers = new Dictionary<Type, ComponentMemberData>
	{
${ComponentMemberDataLookup}
	};

	/// <summary>
	/// Returns a component memberData based on the passed <paramref name=""component""/> type; where an memberData cannot be found
	/// null will be returned instead.
	/// </summary>
	/// <param name=""component""></param>
	public static ComponentMemberData GetComponentMemberData(IComponent component)
	{
		return GetComponentMemberData(component.GetType());
	}

	public static ComponentMemberData GetComponentMemberData(Type componentType)
	{
		return componentMembers.TryGetValue(componentType, out var memberData) ? memberData : null;
	}
}
";
		private const string ComponentMemberData_TEMPLATE = @"		{ typeof(${ComponentType}), new ComponentMemberData()
	    {
		    name = ""${Name}"",
			type = typeof(${Type}),
			index = ${Index},
			isSerializable = ${IsSerializable}
	    }}";
		
		private const string ComponentNoMemberData_TEMPLATE = @"		{ typeof(${ComponentType}), new ComponentMemberData()
	    {
	    }}";
	
		private const string ComponentMemberDataClass_TEMPLATE =
			@"
using System;
using Entitas;

public class ComponentMemberData
{
	public int index;
	public string name;
	public Type type;
	public bool isSerializable;
}";


		public override CodeGenFile[] Generate(CodeGeneratorData[] data)
		{
			var lookups = GenerateLookups(
				data
					.OfType<ComponentData>()
					.ToArray());

			var existingFileNames = new HashSet<string>(lookups.Select(file => file.fileName));

			var emptyLookups = GenerateEmptyLookups(
					data
						.OfType<ContextData>()
						.ToArray())
				.Where(file => !existingFileNames.Contains(file.fileName))
				.ToArray();

			return lookups.Concat(emptyLookups).ToArray();
		}

		private CodeGenFile[] GenerateEmptyLookups(ContextData[] data)
		{
			var emptyData = new ComponentData[0];
			return data
				.Select(d => GenerateComponentsLookupClass(d.GetContextName(), emptyData))
				.ToArray();
		}

		private CodeGenFile[] GenerateLookups(ComponentData[] data)
		{
			return new[]
			{
				GenerateComponentMemberDataClass(),
				GenerateComponentsLookupClass(null, data)
			};
		}

		private CodeGenFile GenerateComponentsLookupClass(string contextName, ComponentData[] data)
		{
			var hasMemberData = string.Join(
				",\n",
				data.Where(cd => !cd.GetTypeName().Contains("Listener"))
					.Where(cd => cd.GetMemberData().Length > 0)
					.Select((d, index) => ComponentMemberData_TEMPLATE
						.Replace("${ComponentType}", d.GetTypeName())
						.Replace("${Name}", d.GetMemberData()[0].name)
						.Replace("${Type}", d.GetMemberData()[0].type)
						.Replace("${IsSerializable}", d.GetMemberData()[0].GetType().IsSerializable.ToString().ToLower())
						.Replace("${Index}", 0.ToString()))
					.ToArray());
			
			var noMemberData = string.Join(
				",\n",
				data.Where(cd => !cd.GetTypeName().Contains("Listener"))
					.Where(cd => cd.GetMemberData().Length == 0)
					.Select((d, index) => ComponentNoMemberData_TEMPLATE
						.Replace("${ComponentType}", d.GetTypeName()))
					.ToArray());

			var newValue = hasMemberData.Length > 0 ? hasMemberData : noMemberData;
			if(hasMemberData.Length > 0 && noMemberData.Length > 0)
				newValue = string.Join(",\n", hasMemberData, noMemberData);
			
			var fileContent = TEMPLATE
				.Replace("${Lookup}", "ComponentMemberDataLookup")
				.Replace("${ComponentMemberDataLookup}", newValue);

			return new CodeGenFile(
				"ComponentMemberDataLookup.cs",
				fileContent,
				GetType().FullName);
		}
		
		private CodeGenFile GenerateComponentMemberDataClass()
		{
			return new CodeGenFile(
				"ComponentMemberData.cs",
				ComponentMemberDataClass_TEMPLATE,
				GetType().FullName);
		}
	}
}