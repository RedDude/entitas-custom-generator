﻿using DesperateDevs.CodeGeneration;

namespace Cointoss.ComponentAccessor.CodeGeneration.Plugins
{
    public class ComponentAccessorData : CodeGeneratorData
    {    
        public const string NameKey = "ComponentAccessor.Name";
        public const string GenerateKey = "ComponentAccessor.Generate";
    
        public string Name
        {
            get => (string)this[NameKey];
            set => this[NameKey] = value;
        }
    
        public string GenerateInitial
        {
            get => (string)this[GenerateKey];
            set => this[GenerateKey] = value;
        }
    }
}
