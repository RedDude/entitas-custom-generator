﻿using DesperateDevs.CodeGeneration;

namespace Cointoss.ComponentAccessor.CodeGeneration.Plugins.CopyTo
{
    public class MemberDataExpandedData  : CodeGeneratorData
    {
        public const string NameKey = "MemberDataExpandedData.Name";
        public const string ComponentKey = "MemberDataExpandedData.ComponentKey";
        public const string MemberDataExpandedKey = "MemberDataExpandedData.MemberDataExpanded";

        public MemberDataExpandedData(string component, MemberDataExpanded[] member)
        {
            Component = component;
            MemberDataExpanded = member;
        }
        public string Name
        {
            get => (string)this[NameKey];
            set => this[NameKey] = value;
        }
        
        public string Component
        {
            get => (string)this[ComponentKey];
            set => this[ComponentKey] = value;
        }
        
        public MemberDataExpanded[] MemberDataExpanded
        {
            get => (MemberDataExpanded[])this[MemberDataExpandedKey];
            set => this[MemberDataExpandedKey] = value;
        }
    }
}