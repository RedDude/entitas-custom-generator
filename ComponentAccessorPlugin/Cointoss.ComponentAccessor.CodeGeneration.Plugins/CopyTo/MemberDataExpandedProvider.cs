﻿using System;
using System.Collections.Generic;
using System.Linq;
using DesperateDevs.CodeGeneration;
using DesperateDevs.CodeGeneration.Plugins;
using DesperateDevs.Roslyn;
using DesperateDevs.Serialization;
using DesperateDevs.Utils;
using Entitas;
using Entitas.CodeGeneration.Attributes;
using Entitas.CodeGeneration.Plugins;
using EntitasRedux.Core.Plugins;
using Genesis.Plugin;
using Microsoft.CodeAnalysis;
using PluginUtil = DesperateDevs.Roslyn.CodeGeneration.Plugins.PluginUtil;


namespace Cointoss.ComponentAccessor.CodeGeneration.Plugins.CopyTo
{
    public class MemberDataExpandedComponentDataProvider : IComponentDataProvider, IDataProvider, ICachable, IConfigurable
    {
        private Preferences preferences;
        private CachedNamedTypeSymbol[] _types;
        
        public string name => "Member Data Expanded Provider";
        public int priority => 1;
        public bool runInDryMode { get; }
        
        public Dictionary<string, object> objectCache { get; set; }
        
        readonly ProjectPathConfig _projectPathConfig = new ProjectPathConfig();
        private AssembliesConfig _assembliesConfig;

        public Dictionary<string, string> defaultProperties { get; }
        
        public void Configure(Preferences preferences)
        {
            _projectPathConfig.Configure(preferences);
            _assembliesConfig = new AssembliesConfig();
        }


        public CodeGeneratorData[] GetData()
        {
            var datas = new List<CodeGeneratorData>();
            // MemoryCache c = new MemoryCache();
            
            // var namedTypeSymbols =
            //     _assembliesConfig.FilterTypeSymbols(_memoryCache.GetNamedTypeSymbols());
            // var dontGenerateAttributeName = nameof(DontGenerateAttribute);
            // var generateNamedTypeSymbols = namedTypeSymbols
            //     .Where(namedTypeSymbol => !namedTypeSymbol.HasAttribute(dontGenerateAttributeName));

            // var componentName = nameof(IComponent);
            // var dataFromComponents = generateNamedTypeSymbols
            //     .Where(cachedNamedTypeSymbol => cachedNamedTypeSymbol.ImplementsInterface(componentName))
            //     .Where(cachedNamedTypeSymbol => !cachedNamedTypeSymbol.NamedTypeSymbol.IsAbstract)
            //     .SelectMany(CreateDataForComponents)
            //     .ToArray();

            var componentInterface = nameof(IComponent);

            var types = _types ?? PluginUtil
                .GetCachedProjectParser(objectCache, _projectPathConfig.projectPath)
                .GetTypes()
                .Select(c => new CachedNamedTypeSymbol(c))
                .ToArray();

            var components = types
                .Where(type => type.ImplementsInterface<IComponent>());
            
                foreach (var component in components)
                {

                    var allFields = component.GetPublicMemberData();
                    var data = new MemberDataExpandedData(component.TypeName, allFields.ToArray());
                    datas.Add(data);
                }

                return datas.ToArray();
        }
        
        public void Provide(Type type, ComponentData data)
        {
            var memberData = type.GetPublicMemberInfos()
                .Select(info => new MemberDataExpanded(
                    info.type,
                    info.name))
                .ToArray();

            data.SetMemberDataExpanded(memberData);
        }

    }

    public static class MemberInfosComponentDataExtension
    {
        public const string COMPONENT_MEMBER_DATA = "Component.MemberDataExpanded";
        public static MemberDataExpanded[] GetMemberDataExpanded(
            this ComponentData data) => (MemberDataExpanded[])data[COMPONENT_MEMBER_DATA];
        public static void SetMemberDataExpanded(
            this ComponentData data, MemberDataExpanded[] memberInfos) =>
            data[COMPONENT_MEMBER_DATA] = memberInfos;
    }
}