﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Reflection;
using System.Text;
using Cointoss.ComponentAccessor.CodeGeneration.Plugins.CopyTo;
using DesperateDevs.CodeGeneration;
using Entitas.CodeGeneration.Plugins;


namespace Cointoss.Copy.CodeGeneration.Plugins
{
    internal sealed class CopyEntityLookupGenerator : AbstractGenerator
    {
        public override string name => "Copy Entity (Lookup)";

        private static readonly StringBuilder _SB;
        private static readonly StringBuilder _SB2;
        private static List<string> paramsList = new List<string>();
        
        static CopyEntityLookupGenerator()
        {
            _SB = new StringBuilder();
            _SB2 = new StringBuilder();
        }
        
        const string TEMPLATE =
            @"using System;
using System.Collections.Generic;

public static class ${Lookup} {

    public static readonly Action<${ContextName}Entity, ${ContextName}Entity>[] replaceComponents = {
${componentNamesList}
    };
}
";
        
        const string COPY_ACTION_TEMPLATE =
            @"      ((original, copy) => { ${collections}copy.Replace${ComponentName}(${params}); })";
        
        const string COPY_ACTION_FLAG_TEMPLATE =
            @"      ((original, copy) => { copy.${prefixedComponentName} = true; })";
        
        const string COMPONENT_NAME_TEMPLATE = @"        ""${ComponentName}""";

        private string[] GetMemberCopyTemplateList(string component, MemberData[] memberData,
            MemberDataExpanded[] memberDataExpandedArray)
        {
            _SB.Clear();
            paramsList.Clear();
            for (var i = 0; i < memberDataExpandedArray.Length; i++)
            {
              
                var result = string.Empty;
                // var md = memberData[i];
                var memberDataExpanded = memberDataExpandedArray[i];
                
                // memberDataExpanded.memberType.GetFullTypeName();
                // If we have type information to work with and its a reference type, use more specific copy Template
				
                var memberTypeSymbol = memberDataExpanded.memberTypeSymbol;
                // var memberTypeSymbol = memberDataExpanded.memberType;
                if (memberTypeSymbol != null)
                {
                    result = memberDataExpanded.GetMemberCopyTemplate(memberTypeSymbol);
                    if (!string.IsNullOrEmpty(result))
                    {
                        result = result.Replace("${ComponentName}", component);
                          
                        _SB.AppendLine(result);
                        paramsList.Add(memberDataExpanded.name);
                        continue;
                    }
                
                }

                if (string.IsNullOrEmpty(result))
                {
                    _SB2.Clear();
                    _SB2.Append("original.");
                    _SB2.Append(component);
                    _SB2.Append(".");
                    _SB2.Append(memberDataExpanded.name);
                    paramsList.Add(_SB2.ToString());
                    // result = _SB2.ToString();
                    // result = memberDataExpanded.GetMemberCopyTemplate();
                }
                // else
                // {
                //     result = memberDataExpanded.GetMemberTemplate();
                // }
              
                // _SB.Append("original.");
                // _SB.Append(component);
                // _SB.Append(".");
                // _SB.Append(md.name);
                
                // if (md.type)
                // {
                //     var collectionName = ""collectionBuffer.Keys.Count;
                //     collectionBuffer.Add(i, collectionName);
                //     _SB.Append(result);
                // }
             
                // if (i < memberData.Length - 1)
                // {
                //     _SB.Append(", ");
                // }
                // _SB.Append(result);
                
                // var memberTypeSymbol = md.memberTypeSymbol;
                // var memberTypeSymbol = md.type;
                // if (memberTypeSymbol != null)
                // {
                // result = md.GetMemberCopyTemplate(memberTypeSymbol);
                // }

                // If we don't have a more nuanced copy Template, use straight Template
                // if(string.IsNullOrEmpty(result))
                // {
                //     result = md.GetMemberCopyTemplate();
                // }

                // Append the final copy member result to the method implementation.
                // if (i < memberData.Length - 1)
                // {
                //     _SB.AppendLine(result);
                // }
                // else
                // {
                //     _SB.Append(result);
                // }
            }

            return new []{_SB.ToString(), string.Join(", ", paramsList)} ;
        }

        public override CodeGenFile[] Generate(CodeGeneratorData[] data)
        {
            _SB.Clear();
            var memberDataExpandedData = data
                .OfType<MemberDataExpandedData>()
                .ToArray();
            var lookups = generateLookups(data
                .OfType<ComponentData>()
                .Where(d => d.ShouldGenerateIndex())
                .ToArray(), memberDataExpandedData );
            
            var existingFileNames = new HashSet<string>(lookups.Select(file => file.fileName));
            
            var emptyLookups = generateEmptyLookups(data
                    .OfType<ContextData>()
                    .ToArray(), memberDataExpandedData)
                .Where(file => !existingFileNames.Contains(file.fileName))
                .ToArray();
            
            return lookups.Concat(emptyLookups).ToArray();
        }

        CodeGenFile[] generateEmptyLookups(ContextData[] data, MemberDataExpandedData[] md)
        {
            var emptyData = Array.Empty<ComponentData>();
            return data
                .Select(d => generateComponentsLookupClass(d.GetContextName(), emptyData, md))
                .ToArray();
        }

        CodeGenFile[] generateLookups(ComponentData[] data, MemberDataExpandedData[] memberDataExpandedDatas)
        {
            var contextNameToComponentData = data
                .Aggregate(new Dictionary<string, List<ComponentData>>(), (dict, d) =>
                {
                    var contextNames = d.GetContextNames();
                    foreach (var contextName in contextNames)
                    {
                        if (!dict.ContainsKey(contextName))
                            dict.Add(contextName, new List<ComponentData>());

                        dict[contextName].Add(d);
                    }

                    return dict;
                });

            foreach (var key in contextNameToComponentData.Keys.ToArray())
            {
                contextNameToComponentData[key] = contextNameToComponentData[key]
                    .OrderBy(d => d.GetTypeName())
                    .ToList();
            }

            return contextNameToComponentData
                .Select(kv => generateComponentsLookupClass(kv.Key, kv.Value.ToArray(), memberDataExpandedDatas))
                .ToArray();
        }

        CodeGenFile generateComponentsLookupClass(string contextName, ComponentData[] data,
            MemberDataExpandedData[] memberDataExpandedDatas)
        {
            memberDataExpandedDatas = memberDataExpandedDatas.OrderBy(m => m.Component)
                .ToArray();
            var componentNamesList = string.Join(",\n", data
                .Select(d =>
                {
                    var mdeData = memberDataExpandedDatas.First(mde =>
                        mde.Component == d.GetTypeName()
                        );
                    var memberDataExpanded = mdeData.MemberDataExpanded;
                    var memberData = d.GetMemberData();
                    // var memberDataExpanded = d.GetMemberDataExpanded();
                    if (memberData.Length == 0)
                        return COPY_ACTION_FLAG_TEMPLATE.Replace("${prefixedComponentName}", d.PrefixedComponentName());

                    var statementTemplate = COPY_ACTION_TEMPLATE.Replace("${ComponentName}", d.ComponentName())
                        .Replace("${ContextName}", contextName);

                    var memberInfos = GetMemberCopyTemplateList(
                        d.ComponentNameValidLowercaseFirst(),
                        memberData, memberDataExpanded);
                    
                    statementTemplate = statementTemplate.Replace("${collections}",
                        string.IsNullOrEmpty(memberInfos[0]) ? "\t" : memberInfos[0]);
                    
                    statementTemplate = statementTemplate.Replace("${params}",
                        memberInfos[1]);

                    return statementTemplate;
                })
            );
            

            // Replace${ComponentName}(${newMethodParameters});
            var fileContent = TEMPLATE
                .Replace("${Lookup}", contextName + "CopyLookup")
                .Replace("${ContextName}", contextName)
                .Replace("${componentNamesList}", componentNamesList);

            return new CodeGenFile(
                contextName + Path.DirectorySeparatorChar +
                contextName + "CopyLookup.cs",
                fileContent,
                GetType().FullName
            );
        }
    }
}