﻿using System;
using DesperateDevs.Utils;
using Entitas.CodeGeneration.Plugins;
namespace Cointoss.ComponentAccessor.CodeGeneration.Plugins.CopyTo
{
    public static class CopyToCodeGeneratorExtensions
    {
        public const char BACKTICK_CHAR = '`';
        
        public static string Replace(this string template, MemberDataExpanded memberDataExpanded)
        {
            return template
                .Replace("${MemberName}", memberDataExpanded.name)
                .Replace("${MemberNameUpper}", memberDataExpanded.name.UppercaseFirst())
                .Replace("${MemberCompilableString}", memberDataExpanded.compilableTypeString);
        }
        
        public static string GetFullTypeName(this Type type)
        {
            var result = type.FullName;
            if (!type.IsGenericType) return result.Replace('+', '.');
            var backTickIndex = result.IndexOf(BACKTICK_CHAR);
            if (backTickIndex > 0)
            {
                result = result.Remove(backTickIndex);
            }

            result += "<";
            var genericTypeParameters = type.GetGenericArguments();
            for (var i = 0; i < genericTypeParameters.Length; ++i)
            {
                var typeParamName = genericTypeParameters[i].GetFullTypeName();
                result += (i == 0 ? typeParamName : "," + typeParamName);
            }
            result += ">";

            return result.Replace('+', '.');
        }
    }

}