﻿using System.Collections.Generic;
using System.Linq;
using Cointoss.ComponentAccessor.CodeGeneration.Plugins.CopyTo;
using Entitas.CodeGeneration.Plugins;
using Genesis.Plugin;
using Microsoft.CodeAnalysis;

namespace EntitasRedux.Core.Plugins
{
    /// <summary>
    /// Helper methods for <see cref="ICachedNamedTypeSymbol"/>.
    /// </summary>
    public static class CachedNamedTypeSymbolExtensions
    {
        public static IEnumerable<MemberDataExpanded> GetPublicMemberData(this ICachedNamedTypeSymbol cachedNamedTypeSymbol)
        {
            // Get all public fields and create members for each.
            var publicFieldMembers = cachedNamedTypeSymbol.AllPublicMembers
                .Where(x =>
                    x.IsPublic() &&
                    x.IsKind(SymbolKind.Field) &&
                    !x.IsStatic)
                .Cast<IFieldSymbol>();

            var publicPropertyMembers = cachedNamedTypeSymbol.AllPublicMembers
                .Where(x =>
                    x.IsPublic() &&
                    x.IsKind(SymbolKind.Property) &&
                    !x.IsStatic)
                .Cast<IPropertySymbol>()
                .Where(x => x.GetMethod != null && x.SetMethod != null);

            // Create member data
            var memberData = publicFieldMembers
                .Select(x => new MemberDataExpanded(x, x.Type, x.Name))
                .Concat(publicPropertyMembers.Select(x => new MemberDataExpanded(
                    x.Type.GetFullTypeName(),
                    x.Name)))
                .ToArray();

            return memberData;
        }
    }
}