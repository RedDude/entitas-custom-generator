﻿using System;
using System.Collections.Generic;
using DesperateDevs.Utils;
using Entitas.CodeGeneration.Plugins;

namespace Cointoss.ComponentAccessor.CodeGeneration.Plugins.CopyTo
{
	/// <summary>
	/// Additional extension methods for <see cref="MemberData"/>
	/// </summary>     
	public static class MemberDataAssigmentExtensions
	{
		// Code templates
		private const string MEMBER_ASSIGNMENT
			= "component.${MemberName} = new${MemberNameUpper};";

		private const string DEFAULT_MEMBER_COPY_ASSIGNMENT
			= "component.${MemberName} = copyComponent.${MemberName};";

		private const string MEMBER_CLONE_ASSIGNMENT
			= "component.${MemberName} = (${MemberCompilableString})copyComponent.${MemberName}.Clone();";

		private const string LIST_MEMBER_DEEP_COPY_ASSIGNMENT
			= "component.${MemberName} = (${MemberCompilableString})JCMG.EntitasRedux.ListTools.DeepCopy(copyComponent.${MemberName});";

		private const string LIST_MEMBER_SHALLOW_COPY_ASSIGNMENT
			= "component.${MemberName} = (${MemberCompilableString})JCMG.EntitasRedux.ListTools.ShallowCopy(copyComponent.${MemberName});";

		private const string DICTIONARY_MEMBER_SHALLOW_COPY_ASSIGNMENT
			= "component.${MemberName} = (${MemberCompilableString})JCMG.EntitasRedux.DictionaryTools.ShallowCopy(copyComponent.${MemberName});";

		private const string DICTIONARY_MEMBER_DEEP_COPY_ASSIGNMENT
			= "component.${MemberName} = (${MemberCompilableString})JCMG.EntitasRedux.DictionaryTools.DeepCopy(copyComponent.${MemberName});";

		private const string DICTIONARY_MEMBER_LIST_DEEP_COPY_ASSIGNMENT
			= "component.${MemberName} = (${MemberCompilableString})JCMG.EntitasRedux.DictionaryTools.DeepCopyListValue(copyComponent.${MemberName});";

		private const string DICTIONARY_MEMBER_ARRAY_DEEP_COPY_ASSIGNMENT
			= "component.${MemberName} = (${MemberCompilableString})JCMG.EntitasRedux.DictionaryTools.DeepCopyArrayValue(copyComponent.${MemberName});";

		private const string ARRAY_MEMBER_DEEP_COPY_ASSIGNMENT
			= "component.${MemberName} = (${MemberCompilableString})JCMG.EntitasRedux.ArrayTools.DeepCopy(copyComponent.${MemberName});";

		private const string ARRAY_MEMBER_SHALLOW_COPY_ASSIGNMENT
			= "component.${MemberName} = (${MemberCompilableString})copyComponent.${MemberName}.Clone();";

		/// <summary>
		/// Returns a snippet of code for direct member assignment for <paramref name="memberData"/> from a parameter.
		/// </summary>
		public static string GetMemberAssignment(this MemberDataExpanded memberData)
		{
			return MEMBER_ASSIGNMENT.Replace(memberData);
		}

		/// <summary>
		/// Returns a snippet of code for direct member assignment for <paramref name="memberData"/> from another
		/// component.
		/// </summary>
		public static string GetMemberCopyAssignment(this MemberDataExpanded memberData)
		{
			return DEFAULT_MEMBER_COPY_ASSIGNMENT.Replace(memberData);
		}

		/// <summary>
		/// Returns a snippet of code for copy member assignment for <paramref name="memberData"/>;  this may be a shallow
		/// or deep copy assignment snippet depending on the <paramref name="memberType"/> or it's generic types if present,
		/// and whether or not they implement <see cref="ICloneable"/>.
		/// </summary>
		public static string GetMemberCopyAssignment(this MemberDataExpanded memberData, Type memberType)
		{
			var result = string.Empty;

			// In this case, a mutable reference type
			var isUnityObjectType = memberType.IsAssignableFrom(typeof(UnityEngine.Object));
			if (memberType == null || !memberType.IsMutableReferenceType() || isUnityObjectType) return result;
			var hasDefaultConstructor = memberType.HasDefaultConstructor();
			if (memberType.IsList(out var genericType) && hasDefaultConstructor)
			{
				result = GetListMemberCopyAssignment(memberData, genericType);
			}
			else if (memberType.IsDictionary(out var genericKeyType, out var genericValueType) &&
			         hasDefaultConstructor)
			{
				result = GetDictionaryMemberCopyAssignment(memberData, genericKeyType, genericValueType);
			}
			else if (memberType.IsArray)
			{
				var elementType = memberType.GetElementType();
				result = GetArrayMemberCopyAssignment(memberData, elementType);
			}
			// Otherwise if the member type implements ICloneable, clone the instance as assignment
			else if (memberType.ImplementsInterface<ICloneable>())
			{
				result = MEMBER_CLONE_ASSIGNMENT.Replace(memberData);
			}

			return result;
		}

		/// <summary>
		/// Returns the member copy assignment code snippet for a member of <see cref="List{T}"/> type.
		/// </summary>
		public static string GetListMemberCopyAssignment(MemberDataExpanded memberData, Type genericType)
		{
			var result = string.Empty;
			if (genericType.IsMutableReferenceType() && genericType.ImplementsInterface<ICloneable>())
			{
				result = LIST_MEMBER_DEEP_COPY_ASSIGNMENT.Replace(memberData);
			}
			else
			{
				result = LIST_MEMBER_SHALLOW_COPY_ASSIGNMENT.Replace(memberData);
			}

			return result;
		}

		/// <summary>
		/// Returns the member copy assignment code snippet for a member of <see cref="Dictionary{TKey,TValue}"/> type.
		/// </summary>
		public static string GetDictionaryMemberCopyAssignment(
			MemberDataExpanded memberData,
			Type genericKeyType,
			Type genericValueType)
		{
			var result = string.Empty;
			// Check for more specific deep copy implementation potential
			if (genericValueType.IsList(out var genericListType) &&
			    genericListType.IsMutableReferenceType() &&
			    genericListType.ImplementsInterface<ICloneable>())
			{
				result = DICTIONARY_MEMBER_LIST_DEEP_COPY_ASSIGNMENT.Replace(memberData);
			}
			else if (genericValueType.IsArray)
			{
				var elementType = genericValueType.GetElementType();
				if (elementType.IsMutableReferenceType() && elementType.ImplementsInterface<ICloneable>())
				{
					result = DICTIONARY_MEMBER_ARRAY_DEEP_COPY_ASSIGNMENT.Replace(memberData);
				}
			}

			// If we can't find a more specific deep copy...
			if (!string.IsNullOrEmpty(result)) return result;
			
			// ...and the value type is capable of deep copy, use a 1-level deep copy,...
			if (genericValueType.IsMutableReferenceType() &&
			    genericValueType.ImplementsInterface<ICloneable>())
			{
				result = DICTIONARY_MEMBER_DEEP_COPY_ASSIGNMENT.Replace(memberData);
			}
			// ...otherwise if the value ends up not being capable of a deep copy, use shallow copy instead
			else
			{
				result = DICTIONARY_MEMBER_SHALLOW_COPY_ASSIGNMENT.Replace(memberData);
			}

			return result;
		}

		/// <summary>
		/// Returns the member copy assignment code snippet for a member of <see cref="Array"/> type.
		/// </summary>
		public static string GetArrayMemberCopyAssignment(MemberDataExpanded memberData, Type elementType)
		{
			var result = string.Empty;
			if (elementType.IsMutableReferenceType() && elementType.ImplementsInterface<ICloneable>())
			{
				result = ARRAY_MEMBER_DEEP_COPY_ASSIGNMENT.Replace(memberData);
			}
			else
			{
				result = ARRAY_MEMBER_SHALLOW_COPY_ASSIGNMENT.Replace(memberData);
			}

			return result;
		}
		
		// public const string COMPONENT_MEMBER_DATA = "Component.MemberData";
		//
		// public static MemberDataExpanded[] GetMemberDataExpanded(this ComponentData data) => (MemberDataExpanded[]) data["Component.MemberDataExpanded"];
		//
		// public static void SetMemberDataExpanded(this ComponentData data, MemberDataExpanded[] memberInfos) => data["Component.MemberDataExpanded"] = (object) memberInfos;
	}
}
