﻿using System;
using System.Collections.Generic;
using DesperateDevs.Utils;
using Entitas.CodeGeneration.Plugins;
using Genesis.Plugin;
using Microsoft.CodeAnalysis;

namespace Cointoss.ComponentAccessor.CodeGeneration.Plugins.CopyTo
{
	/// <summary>
	/// Additional extension methods for <see cref="MemberData"/>
	/// </summary>     
	public static class MemberDataCopyExtensions
	{
		
		private const string MEMBER_TEMPLATE
			= "var ${MemberName} = original.${ComponentName}.${MemberName};";
		
		private const string MEMBER_CLONE_TEMPLATE
			= "       var ${MemberName} = (${MemberCompilableString})original.${MemberName}.Clone();";

		private const string LIST_MEMBER_DEEP_COPY_TEMPLATE
			// = "component.${MemberName} = (${MemberCompilableString})JCMG.EntitasRedux.ListTools.DeepCopy(original.${MemberName});";
			= "        var ${MemberName} =  (${MemberCompilableString})JCMG.EntitasRedux.ListTools.DeepCopy(original.${ComponentName}.${MemberName});";

		private const string LIST_MEMBER_SHALLOW_COPY_TEMPLATE
			= "        var ${MemberName} = (${MemberCompilableString})JCMG.EntitasRedux.ListTools.ShallowCopy(original.${ComponentName}.${MemberName});";

		private const string DICTIONARY_MEMBER_SHALLOW_COPY_TEMPLATE
			= "       var ${MemberName} = (${MemberCompilableString})JCMG.EntitasRedux.DictionaryTools.ShallowCopy(original.${ComponentName}.${MemberName});";

		private const string DICTIONARY_MEMBER_DEEP_COPY_TEMPLATE
			= "        var ${MemberName} = (${MemberCompilableString})JCMG.EntitasRedux.DictionaryTools.DeepCopy(original.${ComponentName}.${MemberName});";

		private const string DICTIONARY_MEMBER_LIST_DEEP_COPY_TEMPLATE
			= "        var ${MemberName} = (${MemberCompilableString})JCMG.EntitasRedux.DictionaryTools.DeepCopyListValue(original.${ComponentName}.${MemberName});";

		private const string DICTIONARY_MEMBER_ARRAY_DEEP_COPY_TEMPLATE
			= "       var ${MemberName} = (${MemberCompilableString})JCMG.EntitasRedux.DictionaryTools.DeepCopyArrayValue(original.${ComponentName}.${MemberName});";

		private const string ARRAY_MEMBER_DEEP_COPY_TEMPLATE
			= "       var ${MemberName} = (${MemberCompilableString})JCMG.EntitasRedux.ArrayTools.DeepCopy(original.${ComponentName}.${MemberName});";

		private const string ARRAY_MEMBER_SHALLOW_COPY_TEMPLATE
			// = "component.${MemberName} = (${MemberCompilableString})original.${MemberName}.Clone();";
			= "        var ${MemberName} = (${MemberCompilableString})original.${ComponentName}.${MemberName};";

		
		private const string HASHSET_MEMBER_SHALLOW_COPY_TEMPLATE
			= "        var ${MemberName} = new HashSet<${memberType}>(); ${MemberName}.ShallowCopy<${memberType}>(original.${ComponentName}.${MemberName});";

		// private const string HASHSET_MEMBER_DEEP_COPY_TEMPLATE
		// 	= "        var ${MemberName} = new HashSet<${memberType}>(); ${MemberName}.DeepCopy(original.${ComponentName}.${MemberName});";

		
		/// <summary>
		/// Returns a snippet of code for direct member Template for <paramref name="memberData"/> from a parameter.
		/// </summary>
		// public static string GetMemberTemplate(this MemberDataExpanded memberData)
		// {
		// 	return MEMBER_TEMPLATE.Replace(memberData);
		// }
		//
		// /// <summary>
		// /// Returns a snippet of code for direct member Template for <paramref name="memberData"/> from another
		// /// component.
		// /// </summary>
		// public static string GetMemberCopyTemplate(this MemberDataExpanded memberData)
		// {
		// 	return DEFAULT_MEMBER_COPY_TEMPLATE.Replace(memberData);
		// }

		
		public static string GetMemberCopyTemplate(this MemberDataExpanded memberData, ITypeSymbol memberTypeSymbol)
		{
			var result = string.Empty;
			var hasDefaultConstructor = memberTypeSymbol.HasDefaultConstructor();
			// var isUnityObjectType = memberTypeSymbol.IsAssignableFrom(typeof(UnityEngine.Object));
			
			// If it's a mutable reference type and not a native Unity Object type, we can likely generate some custom
			// copy code.
			if (!memberTypeSymbol.IsMutableReferenceType()
				// || memberTypeSymbol.IsUnityObject()
			) return result;
			// if (memberTypeSymbol.IsArrayType()
			// {
			// 	result = GetListMemberCopyTemplate(memberData, genericListElementType);
			// }
			if (memberTypeSymbol.IsList(out var genericListElementType) && hasDefaultConstructor)
			{
				result = GetListMemberCopyTemplate(memberData, genericListElementType);
			}
			else if (memberTypeSymbol.IsDictionary(out var genericKeyType, out var genericValueType) &&
			         hasDefaultConstructor)
			{
				result = GetDictionaryMemberCopyTemplate(memberData, genericKeyType, genericValueType);
			}
			else if (memberTypeSymbol.IsHashSet(out genericKeyType) &&
			         hasDefaultConstructor)
			{
				result = GetHashSetMemberCopyTemplate(memberData, genericKeyType);
			}
			// We only have deep-copy support for rank-1 arrays, otherwise ICloneable.Clone will be used since all
			// arrays implement ICloneable.
			else if (memberTypeSymbol is IArrayTypeSymbol arrayMemberTypeSymbol &&
			         arrayMemberTypeSymbol.Rank == 1)
			{
				var elementType = arrayMemberTypeSymbol.ElementType;
				result = GetArrayMemberCopyTemplate(memberData, elementType);
			}
			// Otherwise if the member type implements ICloneable, clone the instance as Template
			else if (memberTypeSymbol.ImplementsInterface<ICloneable>())
			{
				result = MEMBER_CLONE_TEMPLATE.Replace(memberData);
			}
			else
			{
				result = MEMBER_TEMPLATE.Replace(memberData);
			}

			return result;
		}
		
		/// <summary>
		/// Returns the member copy Template code snippet for a member of <see cref="List{T}"/> type.
		/// </summary>
		public static string GetListMemberCopyTemplate(MemberDataExpanded memberData, ITypeSymbol genericType)
		{
			var result = string.Empty;
			result = 
				genericType.IsMutableReferenceType() && genericType.ImplementsInterface<ICloneable>()
					? LIST_MEMBER_DEEP_COPY_TEMPLATE
					: LIST_MEMBER_SHALLOW_COPY_TEMPLATE;

			return result.Replace(memberData).Replace("${memberType}", genericType.Name);
		}

		/// <summary>
		/// Returns the member copy Template code snippet for a member of <see cref="Dictionary{TKey,TValue}"/> type.
		/// </summary>
		public static string GetDictionaryMemberCopyTemplate(
			MemberDataExpanded memberData,
			ITypeSymbol genericKeyType,
			ITypeSymbol genericValueTypeSymbol)
		{
			var result = string.Empty;
			// Check for more specific deep copy implementation potential
			if (genericValueTypeSymbol.IsList(out var genericListType) &&
			    genericListType.IsMutableReferenceType() &&
			    genericListType.ImplementsInterface<ICloneable>())
			{
				result = DICTIONARY_MEMBER_LIST_DEEP_COPY_TEMPLATE.Replace(memberData);
			}
			else if (genericValueTypeSymbol.IsArrayType())
			{
				var elementTypeSymbol = ((IArrayTypeSymbol)genericValueTypeSymbol).ElementType;
				if (elementTypeSymbol.IsReferenceType && elementTypeSymbol.ImplementsInterface<ICloneable>())
				{
					result = DICTIONARY_MEMBER_ARRAY_DEEP_COPY_TEMPLATE.Replace(memberData);
				}
			}

			// If we can't find a more specific deep copy...
			if (!string.IsNullOrEmpty(result)) return result;
			// ...and the value type is capable of deep copy, use a 1-level deep copy,...
			if (genericValueTypeSymbol.IsMutableReferenceType() &&
			    genericValueTypeSymbol.ImplementsInterface<ICloneable>())
			{
				result = DICTIONARY_MEMBER_DEEP_COPY_TEMPLATE.Replace(memberData);
			}
			// ...otherwise if the value ends up not being capable of a deep copy, use shallow copy instead
			else
			{
				result = DICTIONARY_MEMBER_SHALLOW_COPY_TEMPLATE.Replace(memberData);
			}

			return result;
		}
		
		public static string GetHashSetMemberCopyTemplate(MemberDataExpanded memberData, ITypeSymbol genericType)
		{
			var result = string.Empty;
			result = 
				genericType.IsMutableReferenceType() && genericType.ImplementsInterface<ICloneable>()
					? HASHSET_MEMBER_SHALLOW_COPY_TEMPLATE
					: HASHSET_MEMBER_SHALLOW_COPY_TEMPLATE;

			return result.Replace(memberData).Replace("${memberType}", genericType.Name);
		}
		
		/// <summary>
		/// Returns the member copy Template code snippet for a member of <see cref="Array"/> type.
		/// </summary>
		public static string GetArrayMemberCopyTemplate(MemberDataExpanded memberData, ITypeSymbol elementType)
		{
			var result = string.Empty;
			result = 
				elementType.IsMutableReferenceType() && elementType.ImplementsInterface<ICloneable>()
					? ARRAY_MEMBER_DEEP_COPY_TEMPLATE
					: ARRAY_MEMBER_SHALLOW_COPY_TEMPLATE;
			return result.Replace(memberData);
		}
		
		// public const string COMPONENT_MEMBER_DATA = "Component.MemberData";
		//
		// public static MemberDataExpanded[] GetMemberDataExpanded(this ComponentData data) => (MemberDataExpanded[]) data["Component.MemberDataExpanded"];
		//
		// public static void SetMemberDataExpanded(this ComponentData data, MemberDataExpanded[] memberInfos) => data["Component.MemberDataExpanded"] = (object) memberInfos;
	}
}
