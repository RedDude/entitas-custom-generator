﻿using System;
using DesperateDevs.Roslyn;
using DesperateDevs.Utils;
using Microsoft.CodeAnalysis;

namespace Cointoss.ComponentAccessor.CodeGeneration.Plugins.CopyTo
{
    public class MemberDataExpanded
    {
        public readonly IFieldSymbol memberFieldSymbol;
        public readonly ITypeSymbol memberTypeSymbol;
        public readonly Type memberType;
        public readonly string name;
        public readonly string compilableTypeString;

        public MemberDataExpanded(Type type, string memberName)
        {
            memberType = type;
            compilableTypeString = memberType.ToCompilableString();
            name = memberName;
        }

        public MemberDataExpanded(string typeString, string memberName)
        {
            compilableTypeString = typeString;
            name = memberName;
        }

        public MemberDataExpanded(IFieldSymbol fieldSymbol, ITypeSymbol typeSymbol, string memberName)
        {
            memberFieldSymbol = fieldSymbol;
            memberTypeSymbol = typeSymbol;
            compilableTypeString = typeSymbol.ToCompilableString();
            name = memberName;
        }
    }
}