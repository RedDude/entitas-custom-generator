﻿using System;

namespace Cointoss.ComponentAccessor.CodeGeneration.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class DontCreateComponentAccessorAttribute : Attribute
    {
        bool Initial { get; set; }
        
        public DontCreateComponentAccessorAttribute(bool initial)
        {
            Initial = initial;
        }
    }
}