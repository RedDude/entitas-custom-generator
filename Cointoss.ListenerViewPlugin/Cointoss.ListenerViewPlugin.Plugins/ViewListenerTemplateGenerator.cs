﻿using System.IO;
using System.Linq;
using DesperateDevs.CodeGeneration;
using Entitas.CodeGeneration.Attributes;
using Entitas.CodeGeneration.Plugins;

namespace Cointoss.ViewListener.CodeGeneration.Plugins
{
    public class ViewListenerTemplateGenerator : ICodeGenerator
    {

        public string name => "ListenerView";
        public int priority => 0;
        public bool runInDryMode => true;

        private const string TEMPLATE =
@"using Entitas;
using UnityEngine;

//public class ${ComponentName}${EventTarget}${EventType}${ContextName}ViewListener : ${ComponentName}${EventTarget}${EventType}${ContextName}ViewListenerAbstract {
//    
//    public override void On${EventTarget}${ComponentName}${EventType}(${ContextName}Entity entity${methodParameters}) {
//    
//    }
//}
";

    public CodeGenFile[] Generate(CodeGeneratorData[] data)
    {
        return data
            .OfType<ComponentData>()
            .Where(d => d.IsEvent())
            .SelectMany(Generate)
            .ToArray();
    }

    CodeGenFile[] Generate(ComponentData data)
    {
        return data.GetContextNames()
            .SelectMany(contextName => Generate(contextName, data))
            .ToArray();
    }

    CodeGenFile[] Generate(string contextName, ComponentData data)
    {
        return data.GetEventData()
            .Select(eventData =>
            {
                var memberData = data.GetMemberData();
                if (memberData.Length == 0)
                {
                    memberData = new[] {new MemberData("bool", data.PrefixedComponentName())};
                }

                var fileContent = TEMPLATE
                    .Replace("${methodParameters}",
                        data.GetEventMethodArgs(eventData, ", " + memberData.GetMethodParameters(false)))
                    .Replace(data, contextName, eventData);

                fileContent = fileContent.Replace("${EventTarget}", eventData.eventTarget != EventTarget.Self 
                    ? eventData.eventTarget.ToString()
                    : "");
                    
                return new CodeGenFile(
                    "Templates" + Path.DirectorySeparatorChar +
                    "ViewListener" + Path.DirectorySeparatorChar +
                    "I" + data.EventListener(contextName, eventData) + "ViewListenerTemplate.cs",
                    fileContent,
                    GetType().FullName
                );
            }).ToArray();
        }
    }
}
