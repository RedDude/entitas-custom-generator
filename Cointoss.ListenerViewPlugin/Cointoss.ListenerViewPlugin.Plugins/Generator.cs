// using System.IO;
// using System.Linq;
// using DesperateDevs.CodeGeneration;
// using Entitas.CodeGeneration.Plugins;
//
// namespace Cointoss.ViewListener.CodeGeneration.Plugins
// {
//     public class Generator : ICodeGenerator
//     {
//         public string name => "ListenerView";
//         public int priority => 0;
//         public bool runInDryMode => true;
//
//         private const string DIRECTORY_NAME = "ListenerViews";
//         private const string DIRECTORY_SCRIPTS_NAME = "scripts/ListenerViews";
//
//         private const string TEMPLATE =
//             @"using Entitas;
// using UnityEngine;
//
// public partial class {ComponentName}{ContextName}ViewListener : MonoBehaviour, IListenerView, I${ComponentName}Listener {
//     
//     GameEntity _entity;
//  
//     public void RegisterListeners(IEntity entity) {
//         _entity = (GameEntity)entity;
//         _entity.Add${ComponentName}Listener(this);
//     }
// }
// ";
//
//         private const string TEMPLATE_INITIAL =
//             @"using Entitas;
// using UnityEngine;
//
// public partial class ${ComponentName}ViewListener : I${ComponentName}Listener {
//     
// }
// ";
//
//         public CodeGenFile[] Generate(CodeGeneratorData[] data)
//         {
//             var listeners = data
//                 .OfType<ListenerViewData>()
//                 .Select(Generate)
//                 .ToList();
//
//             // var initial = data
//             //     .OfType<ListenerViewData>()     
//             //     .Select(GenerateInitial)
//             //     .Where(c => c != null)
//             //     .ToList();
//
//             // listeners.AddRange(initial);
//             return listeners.ToArray();
//         }
//
//         private CodeGenFile GenerateInitial(ListenerViewData viewData)
//         {
//             var componentName = viewData.Name.ToComponentName(true);
//             var className = componentName + "ViewListener";
//             var filename = DIRECTORY_SCRIPTS_NAME + Path.DirectorySeparatorChar + className + ".cs";
//
//             if (!(bool) viewData[viewData.GenerateInitial])
//             {
//                 return null;
//             }
//
//             var fileContent = TEMPLATE_INITIAL
//                 .Replace("${ClassName}", className)
//                 .Replace("${ComponentName}", componentName);
//
//             return new CodeGenFile(
//                 filename,
//                 fileContent,
//                 GetType().FullName);
//         }
//
//         private CodeGenFile Generate(ListenerViewData viewData)
//         {
//             var componentName = viewData.Name.ToComponentName(true);
//             var className = componentName + "ViewListener";
//             var filename = DIRECTORY_NAME + Path.DirectorySeparatorChar + className + ".cs";
//
//             var fileContent = TEMPLATE
//                 .Replace("${ClassName}", className)
//                 .Replace("${ComponentName}", componentName);
//
//             return new CodeGenFile(
//                 filename,
//                 fileContent,
//                 GetType().FullName);
//         }
//     }
// }