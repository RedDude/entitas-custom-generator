using System.Collections.Generic;
using System.Linq;
using Cointoss.ViewListener.CodeGeneration.Attributes;
using DesperateDevs.CodeGeneration;
using DesperateDevs.CodeGeneration.Plugins;
using DesperateDevs.Roslyn;
using DesperateDevs.Serialization;
using Microsoft.CodeAnalysis;

namespace Cointoss.ViewListener.CodeGeneration.Plugins
{
    public class DataProvider : IDataProvider, IConfigurable, ICachable
    {
        public string name => "ListenerView"; // set this to match your dataprovider name
        public int priority => 0; // can be left at 0
        public bool runInDryMode => true; // allows this to run in dry-run mode, optional
    
        public Dictionary<string, string> defaultProperties => _projectPathConfig.defaultProperties;
        public Dictionary<string, object> objectCache { get; set; }        
        private readonly ProjectPathConfig _projectPathConfig = new ProjectPathConfig();
    
        public void Configure(Preferences preferences)
        {
            _projectPathConfig.Configure(preferences);
        }

        public CodeGeneratorData[] GetData()
        {
            //ComponentDataProvider
            return DesperateDevs.Roslyn.CodeGeneration.Plugins.PluginUtil
                .GetCachedProjectParser(objectCache, _projectPathConfig.projectPath)
                .GetTypes()
                .Where(type => ImplementsInterface(type, "IComponent"))
                .Where(type => type.GetAttribute<ListenerViewAttribute>() != null) // change to your attribute
                .Select(type => new ListenerViewData // change to your data type
                {
                    Name = type.Name,
                }).ToArray();
        }
  
      
        public static bool ImplementsInterface(INamedTypeSymbol typeSymbol, string interfaceName)
        {
            if (typeSymbol == null)
            {
                return false;
            }

            foreach (var @interface in typeSymbol.AllInterfaces)
            {
                if (@interface.MetadataName == interfaceName)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
