﻿using DesperateDevs.CodeGeneration;

namespace Cointoss.ViewListener.CodeGeneration.Plugins
{
    public class ListenerViewData : CodeGeneratorData
    {    
        public const string NameKey = "ListernerView.Name";
        public const string GenerateKey = "ListernerView.Generate";
    
        public string Name
        {
            get => (string)this[NameKey];
            set => this[NameKey] = value;
        }
    
        public string GenerateInitial
        {
            get => (string)this[GenerateKey];
            set => this[GenerateKey] = value;
        }
    }
}
