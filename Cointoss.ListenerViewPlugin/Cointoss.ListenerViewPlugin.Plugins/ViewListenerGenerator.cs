﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DesperateDevs.CodeGeneration;
using DesperateDevs.Utils;
using Entitas.CodeGeneration.Attributes;
using Entitas.CodeGeneration.Plugins;

namespace Cointoss.ViewListener.CodeGeneration.Plugins
{
    public class ViewListenerGenerator : ICodeGenerator
    {

        public string name => "ListenerView";
        public int priority => 0;
        public bool runInDryMode => true;

        private const string TEMPLATE =
@"using Entitas;
using UnityEngine;

public abstract class ${ComponentName}${EventTarget}${EventType}${ContextName}ViewListenerAbstract : MonoBehaviour, IListenerView, I${EventListener} {
    
    protected ${ContextName}Entity _entity;
    public bool replayListener = true;
    public bool forBaseEntity;

    public bool ForBase { get => forBaseEntity; }

    public virtual IEntity BeforeRegisterListeners(IEntity entity) {
        return entity;
    }
 
    public void RegisterListeners(IEntity entity) {
        _entity = (${ContextName}Entity)BeforeRegisterListeners(entity);

        if(${Condition})
            return;

        _entity.Add${EventListener}(this);
        //if (this is IEntityListenerViewComponentRemoved)
        //{
        //    _entity.Add${EventTarget}${ComponentName}RemovedListener(this);
        //}

        if(replayListener${HasComponentCondition}) {
            On${EventTarget}${ComponentName}${EventType}(_entity${replayParameters});
        }
    }

    public virtual void On${EventTarget}${ComponentName}${EventType}(${ContextName}Entity entity${methodParameters}) {
    }

    //public void On${EventTarget}${ComponentName}Removed(GameEntity entity) {
    //    ((IEntityListenerViewComponentRemoved)this).OnComponentRemoved(entity); 
    //}
    

    public void UnregisterListeners(IEntity entity) {
       if(${Condition})
            return;
        try {
            ((${ContextName}Entity)entity).Remove${EventListener}(this);
            //if (this is IEntityListenerViewComponentRemoved)
            //{
            //    _entity.Add${EventTarget}${ComponentName}RemovedListener(this);
            //}
        } catch {
            //ignore
        }
    }
}
";

    public CodeGenFile[] Generate(CodeGeneratorData[] data)
    {
        return data
            .OfType<ComponentData>()
            .Where(d => d.IsEvent())
            .SelectMany(Generate)
            .ToArray();
    }

    CodeGenFile[] Generate(ComponentData data)
    {
        return data.GetContextNames()
            .SelectMany(contextName => Generate(contextName, data))
            .ToArray();
    }

    CodeGenFile[] Generate(string contextName, ComponentData data)
    {
        return data.GetEventData()
            .Select(eventData =>
            {
                var memberData = data.GetMemberData();
                if (memberData.Length == 0)
                {
                    memberData = new[] {new MemberData("bool", data.PrefixedComponentName())};
                }

                var fileContent = TEMPLATE
                    .Replace("${methodParameters}",
                        data.GetEventMethodArgs(eventData, ", " + memberData.GetMethodParameters(false)))
                    .Replace(data, contextName, eventData);

                fileContent = fileContent.Replace("${EventTarget}", eventData.eventTarget != EventTarget.Self 
                    ? eventData.eventTarget.ToString()
                    : "");
                
                var replayParameters = data.GetMemberData().Length > 0 && eventData.eventType != EventType.Removed 
                    ? ", " + string.Join(",", (memberData).Select(
                        (info => $"_entity.{data.ComponentNameValidLowercaseFirst()}.{info.name}")).ToArray()
                        )
                    : "";
                fileContent = fileContent.Replace("${replayParameters}", replayParameters);

                var hasComponentCondition = eventData.eventTarget == EventTarget.Self
                    ? " && _entity." + (data.GetMemberData().Length == 0
                          ? data.PrefixedComponentName()
                          : ("has" + data.ComponentName()))
                    : "";

                fileContent = fileContent.Replace("${Condition}", $"_entity == null");// + hasComponentCondition);
                fileContent = fileContent.Replace("${HasComponentCondition}", hasComponentCondition);
                    
                return new CodeGenFile(
                    "Events" + Path.DirectorySeparatorChar +
                    "Abstract" + Path.DirectorySeparatorChar +
                    "I" + data.EventListener(contextName, eventData) + "ViewListener.cs",
                    fileContent,
                    GetType().FullName
                );
            }).ToArray();
        }
    }
}
