﻿using System;

namespace Cointoss.ViewListener.CodeGeneration.Attributes
{
    // change attribute usage if desired, 
    // this defines where the attribute is allowed to be placed
    // e.g. on a class, field, property, method etc
    [AttributeUsage(AttributeTargets.Class)]
    public class ListenerViewAttribute : Attribute
    {
        bool Initial { get; set; }
        
        public ListenerViewAttribute(bool initial)
        {
            Initial = initial;
        }
    }
}