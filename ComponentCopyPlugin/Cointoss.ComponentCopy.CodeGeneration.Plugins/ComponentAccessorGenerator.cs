﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DesperateDevs.CodeGeneration;
using DesperateDevs.Utils;
using Entitas.CodeGeneration.Attributes;
using Entitas.CodeGeneration.Plugins;

namespace Cointoss.ComponentAccessor.CodeGeneration.Plugins
{
    	internal sealed class ComponentAccessorGenerator : AbstractGenerator
	{
		public override string name => "ComponentAccessor (Lookup)";

		private const string TEMPLATE =
			@"using System;
using System.Collections.Generic;
using Entitas;

public static class ${Lookup} {

	public static readonly Dictionary<Type, ComponentAccessor> componentAccessorLookup = new Dictionary<Type, ComponentAccessor>
	{
${ComponentAccessorLookup}
	};

	/// <summary>
	/// Returns a component memberData based on the passed <paramref name=""component""/> type; where an memberData cannot be found
	/// null will be returned instead.
	/// </summary>
	/// <param name=""component""></param>
	public static ComponentAccessor GetComponentAccessor(IComponent component)
	{
		return GetComponentAccessor(component.GetType());
	}

	public static ComponentAccessor GetComponentAccessor(Type componentType)
	{
		return componentAccessorLookup.TryGetValue(componentType, out var memberData) ? memberData : null;
	}
}
";
		private const string ComponentAccessor_TEMPLATE = @"		{ typeof(${ComponentType}), new ComponentAccessor()
	    {
		    GetValue = (component) => ((${ComponentType}) component).${Name},
		    SetValue = (component, value) => ((${ComponentType}) component).${Name} = (${Type}) value,
			FieldType = typeof(${Type})
	    }}";

		private const string ComponentAccessorClass_TEMPLATE =
			@"
using System;
using Entitas;
public class ComponentAccessor
{
	public Func<IComponent, object> GetValue;
	public Action<IComponent, object> SetValue;
	public Type FieldType;
 
	public ComponentAccessor() {}
	public ComponentAccessor(Func<IComponent, object> getValue, Action<IComponent, object> setValue, Type fieldType)
	{
		GetValue = getValue;
		SetValue = setValue;
		FieldType = fieldType;
	}
}";

		public override CodeGenFile[] Generate(CodeGeneratorData[] data)
		{
			var lookups = GenerateLookups(
				data
					.OfType<ComponentData>()
					.ToArray());

			var existingFileNames = new HashSet<string>(lookups.Select(file => file.fileName));

			var emptyLookups = GenerateEmptyLookups(
					data
						.OfType<ContextData>()
						.ToArray())
				.Where(file => !existingFileNames.Contains(file.fileName))
				.ToArray();

			return lookups.Concat(emptyLookups).ToArray();
		}

		private CodeGenFile[] GenerateEmptyLookups(ContextData[] data)
		{
			var emptyData = new ComponentData[0];
			return data
				.Select(d => GenerateComponentsLookupClass(d.GetContextName(), emptyData))
				.ToArray();
		}

		private CodeGenFile[] GenerateLookups(ComponentData[] data)
		{
			return new[] { 
				GenerateComponentAccessorClass(),
				GenerateComponentsLookupClass(null, data)}
			;
		}

		private CodeGenFile GenerateComponentsLookupClass(string contextName, ComponentData[] data)
		{
			var componentAccessorLookup = string.Join(
				",\n",
				data.Where(cd => !cd.GetTypeName().Contains("Listener"))
					.Where(cd => cd.GetMemberData().Length > 0)
					.Select((d, index) => ComponentAccessor_TEMPLATE
						.Replace("${ComponentType}", d.GetTypeName())
						.Replace("${Name}", d.GetMemberData()[0].name)
						.Replace("${Type}", d.GetMemberData()[0].type))
					.ToArray());

			var fileContent = TEMPLATE
				.Replace("${Lookup}", "ComponentAccessorLookup")
				.Replace("${ComponentAccessorLookup}", componentAccessorLookup);

			return new CodeGenFile(
				"ComponentAccessorLookup.cs",
				fileContent,
				GetType().FullName);
		}
		
		private CodeGenFile GenerateComponentAccessorClass()
		{
			return new CodeGenFile(
				"ComponentAccessor.cs",
				ComponentAccessorClass_TEMPLATE,
				GetType().FullName);
		}
	}
        
}
