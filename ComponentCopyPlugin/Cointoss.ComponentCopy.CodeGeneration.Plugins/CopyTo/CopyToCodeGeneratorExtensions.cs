﻿using DesperateDevs.Utils;
using Entitas.CodeGeneration.Plugins;
namespace Cointoss.ComponentAccessor.CodeGeneration.Plugins.CopyTo
{
    public static class CopyToCodeGeneratorExtensions
    {
        public static string Replace(this string template, MemberData memberData)
        {
            return template
                .Replace("${MemberName}", memberData.name)
                .Replace("${MemberNameUpper}", memberData.name.UppercaseFirst());
            // .Replace("${MemberCompilableString}", memberData.compilableTypeString);
        }
    }

}