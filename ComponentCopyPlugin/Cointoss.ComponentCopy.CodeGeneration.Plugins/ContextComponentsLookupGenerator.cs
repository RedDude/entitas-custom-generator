﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DesperateDevs.CodeGeneration;
using DesperateDevs.Utils;
using Entitas.CodeGeneration.Attributes;
using Entitas.CodeGeneration.Plugins;

namespace Cointoss.ComponentAccessor.CodeGeneration.Plugins
{
  public class ContextComponentsLookupGenerator : AbstractGenerator {

        public override string name => "ContextComponentsLookup (Lookup)";

        const string TEMPLATE =
            @"using System;
using System.Collections.Generic;
using Entitas;

    public static class ${Lookup} {

    public static readonly Dictionary<string, System.Type[]> componentTypes = new Dictionary<string, System.Type[]>{
    ${ContextComponentsLookup}
    };

    public static readonly Dictionary<string, string[]> componentNames = new Dictionary<string, string[]>{
    ${contextComponentNamesLookup}
    };
    
}
";

        const string CONTEXT_COMPONENT_TYPES_TEMPLATE = @"         { ""${ContextName}"", ${ContextName}ComponentsLookup.componentTypes }";
        const string CONTEXT_COMPONENT_NAMES_TEMPLATE = @"         { ""${ContextName}"", ${ContextName}ComponentsLookup.componentNames }";

        public override CodeGenFile[] Generate(CodeGeneratorData[] data) {
            var lookups = GenerateLookups(data
                .OfType<ContextData>()
                .ToArray());

            return lookups;
        }


        CodeGenFile[] GenerateLookups(ContextData[] data)
        {
            var ContextComponentsLookup = string.Join(",\n", data
                .Select((d, index) => CONTEXT_COMPONENT_TYPES_TEMPLATE
                    .Replace("${ContextName}", d.GetContextName())));
            
            var contextComponentNamesLookup = string.Join(",\n", data
                .Select((d, index) => CONTEXT_COMPONENT_NAMES_TEMPLATE
                    .Replace("${ContextName}", d.GetContextName())));

            var fileContent = TEMPLATE
                .Replace("${Lookup}", "ContextComponentsLookup")
                .Replace("${ContextComponentsLookup}", ContextComponentsLookup)
                .Replace("${contextComponentNamesLookup}", contextComponentNamesLookup);

            return new[]
            {
                new CodeGenFile(
                    "ContextComponentsLookup.cs",
                    fileContent,
                    GetType().FullName
                )
            };
        }
    }
}
