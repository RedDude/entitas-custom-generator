﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DesperateDevs.CodeGeneration;
using DesperateDevs.Utils;
using Entitas.CodeGeneration.Attributes;
using Entitas.CodeGeneration.Plugins;

namespace Cointoss.ComponentAccessor.CodeGeneration.Plugins
{
    public class ComponentMatcherLookupGenerator : AbstractGenerator {

        public override string name { get { return "Component Matcher (Lookup)"; } }

        const string TEMPLATE =
            @"public static class ${Lookup} {
    public static readonly Entitas.IMatcher<${contextName}Entity>[] componentMatchers = {
${componentMatchersList}
    };
}
";

        const string COMPONENT_TYPE_TEMPLATE = @"        ${contextName}Matcher.${ComponentName}";

        public override CodeGenFile[] Generate(CodeGeneratorData[] data) {
            var lookups = generateLookups(data
                .OfType<ComponentData>()
                .Where(d => d.ShouldGenerateIndex())
                .ToArray());

            var existingFileNames = new HashSet<string>(lookups.Select(file => file.fileName));

            var emptyLookups = generateEmptyLookups(data
                    .OfType<ContextData>()
                    .ToArray())
                .Where(file => !existingFileNames.Contains(file.fileName))
                .ToArray();

            return lookups.Concat(emptyLookups).ToArray();
        }

        CodeGenFile[] generateEmptyLookups(ContextData[] data) {
            var emptyData = new ComponentData[0];
            return data
                .Select(d => generateComponentsLookupClass(d.GetContextName(), emptyData))
                .ToArray();
        }

        CodeGenFile[] generateLookups(ComponentData[] data) {
            var contextNameToComponentData = data
                .Aggregate(new Dictionary<string, List<ComponentData>>(), (dict, d) => {
                    var contextNames = d.GetContextNames();
                    foreach (var contextName in contextNames) {
                        if (!dict.ContainsKey(contextName)) {
                            dict.Add(contextName, new List<ComponentData>());
                        }

                        dict[contextName].Add(d);
                    }

                    return dict;
                });

            foreach (var key in contextNameToComponentData.Keys.ToArray()) {
                contextNameToComponentData[key] = contextNameToComponentData[key]
                    .OrderBy(d => d.GetTypeName())
                    .ToList();
            }

            return contextNameToComponentData
                .Select(kv => generateComponentsLookupClass(kv.Key, kv.Value.ToArray()))
                .ToArray();
        }

        CodeGenFile generateComponentsLookupClass(string contextName, ComponentData[] data) {
            var componentTypesList = string.Join(",\n", data
                .Select(d => COMPONENT_TYPE_TEMPLATE
                    .Replace("${contextName}", contextName)
                    .Replace("${ComponentName}", d.ComponentName())
                    .Replace("${ComponentType}", d.GetTypeName())
                ).ToArray());

            var fileContent = TEMPLATE
                .Replace("${contextName}", contextName)
                .Replace("${Lookup}", contextName + "Matcher" + CodeGeneratorExtentions.LOOKUP)
                .Replace("${componentMatchersList}", componentTypesList);

            return new CodeGenFile(
                contextName + Path.DirectorySeparatorChar +
                contextName + "ComponentsMatcherLookup.cs",
                fileContent,
                GetType().FullName
            );
        }
        
    }
        
}
