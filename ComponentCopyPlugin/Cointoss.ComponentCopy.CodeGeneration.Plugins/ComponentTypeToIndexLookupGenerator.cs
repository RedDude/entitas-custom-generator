﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DesperateDevs.CodeGeneration;
using DesperateDevs.Utils;
using Entitas.CodeGeneration.Attributes;
using Entitas.CodeGeneration.Plugins;

namespace Cointoss.ComponentAccessor.CodeGeneration.Plugins
{
	internal sealed class ComponentTypeToIndexLookupGenerator : AbstractGenerator
	{
		public override string name => "ComponentTypeToIndex (Lookup)";

		private const string TEMPLATE =
			@"using System;
using System.Collections.Generic;
using Entitas;

public static class ${Lookup} {

	public static readonly Dictionary<Type, int> ComponentTypeToIndex = new Dictionary<Type, int>
	{
${componentTypeToIndexLookup}
	};

	/// <summary>
	/// Returns a component index based on the passed <paramref name=""component""/> type; where an index cannot be found
	/// -1 will be returned instead.
	/// </summary>
	/// <param name=""component""></param>
	public static int GetComponentIndex(IComponent component)
	{
		return GetComponentIndex(component.GetType());
	}

	/// <summary>
	/// Returns a component index based on the passed <paramref name=""componentType""/>; where an index cannot be found
	/// -1 will be returned instead.
	/// </summary>
	/// <param name=""componentType""></param>
	public static int GetComponentIndex(Type componentType)
	{
		return ComponentTypeToIndex.TryGetValue(componentType, out var index) ? index : -1;
	}
}
";

		private const string COMPONENT_TYPE_TO_INDEX_TEMPLATE = "        { typeof(${ComponentType}), ${Index} }";

		public override CodeGenFile[] Generate(CodeGeneratorData[] data)
		{
			var lookups = GenerateLookups(
				data
					.OfType<ComponentData>()
					.Where(d => d.ShouldGenerateIndex())
					.ToArray());

			var existingFileNames = new HashSet<string>(lookups.Select(file => file.fileName));

			var emptyLookups = GenerateEmptyLookups(
					data
						.OfType<ContextData>()
						.ToArray())
				.Where(file => !existingFileNames.Contains(file.fileName))
				.ToArray();

			return lookups.Concat(emptyLookups).ToArray();
		}

		private CodeGenFile[] GenerateEmptyLookups(ContextData[] data)
		{
			var emptyData = new ComponentData[0];
			return data
				.Select(d => GenerateComponentsLookupClass(d.GetContextName(), emptyData))
				.ToArray();
		}

		private CodeGenFile[] GenerateLookups(ComponentData[] data)
		{
			var contextNameToComponentData = data
				.Aggregate(
					new Dictionary<string, List<ComponentData>>(),
					(dict, d) =>
					{
						var contextNames = d.GetContextNames();
						foreach (var contextName in contextNames)
						{
							if (!dict.ContainsKey(contextName))
							{
								dict.Add(contextName, new List<ComponentData>());
							}

							dict[contextName].Add(d);
						}

						return dict;
					});

			foreach (var key in contextNameToComponentData.Keys.ToArray())
			{
				contextNameToComponentData[key] = contextNameToComponentData[key]
					.OrderBy(d => d.GetTypeName())
					.ToList();
			}

			return contextNameToComponentData
				.Select(kv => GenerateComponentsLookupClass(kv.Key, kv.Value.ToArray()))
				.ToArray();
		}

		private CodeGenFile GenerateComponentsLookupClass(string contextName, ComponentData[] data)
		{
			var componentTypeToIndexLookup = string.Join(
				",\n",
				data
					.Select(
						(d, index) => COMPONENT_TYPE_TO_INDEX_TEMPLATE
							.Replace("${ComponentType}", d.GetTypeName())
							.Replace("${Index}", index.ToString()))
					.ToArray());


			var fileContent = TEMPLATE
				.Replace("${Lookup}", contextName + "ComponentsTypeToIndexLookup")
				.Replace("${componentTypeToIndexLookup}", componentTypeToIndexLookup);

			return new CodeGenFile(
				contextName +
				Path.DirectorySeparatorChar +
				contextName +
				"ComponentTypeToIndexLookup.cs",
				fileContent,
				GetType().FullName);
		}
	}
}

