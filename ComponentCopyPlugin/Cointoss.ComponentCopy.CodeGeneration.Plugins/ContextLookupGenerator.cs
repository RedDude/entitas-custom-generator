﻿using DesperateDevs.Utils;

namespace Cointoss.ComponentAccessor.CodeGeneration.Plugins
{
    using System.Linq;
    using DesperateDevs.CodeGeneration;
    using Entitas.CodeGeneration.Plugins;

    public class ContextLookupGenerator : AbstractGenerator
    {

        public override string name => "Context (Lookup)";

        const string TEMPLATE =
            @"using System;
using System.Collections.Generic;
using Entitas;

    public static class ${Lookup} {

    public static readonly Dictionary<string, System.Type> ContextLookup = new Dictionary<string, System.Type>{
    ${ContextConstantsList}
    };

    public static readonly Dictionary<string, Func<IEntity[]>> ContextGetEntitiesLookup = new Dictionary<string, Func<IEntity[]>>{
    ${ContextGetEntitiesLookup}
    };

    public static readonly Dictionary<string, Func<IEntity>> ContextCreateEntityLookup = new Dictionary<string, Func<IEntity>>{
    ${ContextCreateEntityLookup}
    };
    
${totalContextsConstant}

    public static readonly string[] ContextNames = {
${ContextNamesList}
    };

    public static readonly System.Type[] ContextTypes = {
${ContextTypesList}
    };
}
";

        const string CONTEXT_CONSTANT_TEMPLATE = @"        {""${ContextName}"", typeof(${Func})}";

        const string CONTEXT_GET_ENTITIES_TEMPLATE =
            @"         { ""${ContextName}"", () => Contexts.sharedInstance.${LowerContextName}.GetEntities() }";

        const string CONTEXT_CREATE_ENTITY_TEMPLATE =
            @"         { ""${ContextName}"", () => Contexts.sharedInstance.${LowerContextName}.CreateEntity() }";

        const string TOTAL_CONTEXT_CONSTANT_TEMPLATE = @"    public const int TotalContexts = ${totalContexts};";
        const string CONTEXT_NAME_TEMPLATE = @"        ""${ContextName}""";
        const string CONTEXT_TYPE_TEMPLATE = @"        typeof(${ContextType})";

        public override CodeGenFile[] Generate(CodeGeneratorData[] data)
        {
            var lookups = GenerateLookups(data
                .OfType<ContextData>()
                .ToArray());

            return lookups;
        }

        // CodeGenFile[] GenerateEmptyLookups(ContextData[] data) {
        //     var emptyData = new ContextData[0];
        //     return data
        //         .Select(d => GenerateContextsLookupClass(d.GetContextName(), emptyData))
        //         .ToArray();
        // }

        CodeGenFile[] GenerateLookups(ContextData[] data)
        {

            var contextConstantsList = string.Join(",\n", data
                .Select((d, index) => CONTEXT_CONSTANT_TEMPLATE
                    .Replace("${ContextName}", d.GetContextName())
                    .Replace("${Func}", d.GetContextName() + "Context")));

            var contextGetEntities = string.Join(",\n", data
                .Select((d, index) => CONTEXT_GET_ENTITIES_TEMPLATE
                    .Replace("${ContextName}", d.GetContextName())
                    .Replace("${LowerContextName}", d.GetContextName().LowercaseFirst())));

            var contextCreateEntity = string.Join(",\n", data
                .Select((d, index) => CONTEXT_CREATE_ENTITY_TEMPLATE
                    .Replace("${ContextName}", d.GetContextName())
                    .Replace("${LowerContextName}", d.GetContextName().LowercaseFirst())));

            var totalContextsConstant = TOTAL_CONTEXT_CONSTANT_TEMPLATE
                .Replace("${totalContexts}", data.Length.ToString());

            var contextNamesList = string.Join(",\n", data
                .Select(d => CONTEXT_NAME_TEMPLATE
                    .Replace("${ContextName}", d.GetContextName())
                ).ToArray());

            var contextTypesList = string.Join(",\n", data
                .Select(d => CONTEXT_TYPE_TEMPLATE
                    .Replace("${ContextType}", d.GetContextName() + "Context")
                ).ToArray());

            var fileContent = TEMPLATE
                .Replace("${Lookup}", "ContextsLookup")
                .Replace("${ContextConstantsList}", contextConstantsList)
                .Replace("${ContextGetEntitiesLookup}", contextGetEntities)
                .Replace("${ContextCreateEntityLookup}", contextCreateEntity)

                .Replace("${totalContextsConstant}", totalContextsConstant)
                .Replace("${ContextNamesList}", contextNamesList)
                .Replace("${ContextTypesList}", contextTypesList);

            return new[]
            {
                new CodeGenFile(
                    "ContextsLookup.cs",
                    fileContent,
                    GetType().FullName
                )
            };

            // GenerateContextsLookupClass(kv.Key, new[] {kv.Value})
            // dict.
            // foreach (var key in dict.Keys.ToArray()) {
            //     dict[key] = contextNameToContextData[key]
            //         .OrderBy(d => d.GetTypeName())
            //         .ToList();
            // }

            // return contextNameToContextData
            //     .Select(kv => GenerateContextsLookupClass(kv.Key, new[] {kv.Value}))
            //     .ToArray();
        }

        //
        // CodeGenFile GenerateContextsLookupClass(string contextName, ContextData[] data) {
        //     var ContextConstantsList = string.Join("\n", data
        //         .Select((d, index) => Context_CONSTANT_TEMPLATE
        //             .Replace("${ContextName}", d.GetContextName())
        //             .Replace("${Index}", index.ToString())).ToArray());
        //
        //     var totalContextsConstant = TOTAL_ContextS_CONSTANT_TEMPLATE
        //         .Replace("${totalContexts}", data.Length.ToString());
        //
        //     var ContextNamesList = string.Join(",\n", data
        //         .Select(d => Context_NAME_TEMPLATE
        //             .Replace("${ContextName}", d.GetContextName())
        //         ).ToArray());
        //
        //     var ContextTypesList = string.Join(",\n", data
        //         .Select(d => Context_TYPE_TEMPLATE
        //             .Replace("${ContextType}", d.GetContextName() + "Context")
        //         ).ToArray());
        //
        //     var fileContent = TEMPLATE
        //         .Replace("${Lookup}", contextName + CodeGeneratorExtentions.LOOKUP)
        //         .Replace("${ContextConstantsList}", ContextConstantsList)
        //         .Replace("${totalContextsConstant}", totalContextsConstant)
        //         .Replace("${ContextNamesList}", ContextNamesList)
        //         .Replace("${ContextTypesList}", ContextTypesList);
        //
        //     return new CodeGenFile(
        //         contextName + Path.DirectorySeparatorChar +
        //         contextName + "ContextsLookup.cs",
        //         fileContent,
        //         GetType().FullName
        //     );
        // }
    }
}